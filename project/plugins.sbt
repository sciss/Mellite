addSbtPlugin("com.eed3si9n"     % "sbt-assembly"        % "2.2.0")    // cross-platform standalone
addSbtPlugin("com.eed3si9n"     % "sbt-buildinfo"       % "0.11.0")   // meta-data such as project and Scala version
addSbtPlugin("com.typesafe"     % "sbt-mima-plugin"     % "1.1.3")    // binary compatibility testing
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.8.1")    // release standalone binaries
addSbtPlugin("com.typesafe.sbt" % "sbt-license-report"  % "1.2.0")    // generates list of dependencies licenses
