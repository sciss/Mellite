import com.typesafe.sbt.license.{DepLicense, DepModuleInfo, LicenseInfo}
import com.typesafe.sbt.packager.linux.LinuxPackageMapping
import sbt.Package.ManifestAttributes

lazy val baseName                   = "Mellite"
lazy val baseNameL                  = baseName.toLowerCase
lazy val appDescription             = "A computer music application based on SoundProcesses"
// contains only the minimum set of objects.
// When changing the major version of `coreVersion`, one also has to bump
// `mimaCoreVersion`, `bundleVersion`, `mimaBundleVersion`, and `appVersion`
// in order to ensure binary compatibility.
// Basically: coreVersion <= bundleVersion <= appVersion
// and major(coreVersion) <= major(bundleVersion) == major(appVersion)
lazy val coreVersion                = "3.12.13"
lazy val mimaCoreVersion            = "3.12.0"            // re `core`
// contains the widest possible set of objects.
// When changing the major version of `bundleVersion`, one also has to bump `appVersion` and `mimaBundleVersion`,
// in order to ensure binary compatibility.
lazy val bundleVersion              = "3.14.1"
lazy val mimaBundleVersion          = "3.14.0"            // re `bundle` and `app`
// no-source project depending on `bundle` that can use newer (minor) bundle releases.
// When changing the major version of `appVersion`, one also has to bump `bundleVersion` and `mimaBundleVersion`,
// in order to ensure binary compatibility.
lazy val appVersion                 = "3.14.9"

lazy val loggingEnabled             = true

lazy val authorName                 = "Hanns Holger Rutz"
lazy val authorEMail                = "contact@sciss.de"
lazy val gitRepoHost                = "codeberg.org"
lazy val gitRepoUser                = "sciss"
lazy val gitRepoName                = baseName
lazy val gitIssueTracker            = url(s"https://$gitRepoHost/$gitRepoUser/$gitRepoName/issues")

// ---- changes ----

lazy val changeLog = Seq(
  "Control: add support for AuralSystem.Connect",
)

// ---- dependencies ----

//Global / evictionErrorLevel := Level.Warn

lazy val deps = new {
  val common = new {
    val asyncFile           = Seq("0.2.1")
    val audioFile           = Seq("2.4.2")
    val audioWidgets        = Seq("2.4.3")
    val desktop             = Seq("0.11.4")
    val equal               = Seq("0.1.6")
    val fileUtil            = Seq("1.1.5")
    val lucre               = Seq("4.6.4", "4.6.6")
    val lucreSwing          = Seq("2.10.1")
    val model               = Seq("0.3.5")
    val numbers             = Seq("0.2.1")
    val processor           = Seq("0.5.0")
    val raphaelIcons        = Seq("1.0.7")
    val scalaCollider       = Seq("2.7.4", "2.7.5")
    val scalaColliderUGens  = Seq("1.21.4")
    val scalaColliderIf     = Seq("1.8.0")
    val scalaOSC            = Seq("1.3.1")
    val scalaSTM            = Seq("0.11.1")
    val scalaSwing          = Seq("3.0.0")
    val scallop             = Seq("4.1.0")
    val serial              = Seq("2.0.1")
    val sonogram            = Seq("2.3.0")
    val soundProcesses      = Seq("4.14.8", "4.14.17")
    val span                = Seq("2.0.2")
    val swingPlus           = Seq("0.5.0")
  }
  val app = new {
    val akka                = Seq("2.6.21") // note -- should correspond to FScape always
    val appDirs             = Seq("1.2.2")
    val dejaVuFonts         = Seq("2.37")   // directly included
    val dotterweide         = Seq("0.4.3")
    val fileCache           = Seq("1.1.2")
    val fingerTree          = Seq("1.5.5")
    val freesound           = Seq("2.13.0")
    val fscape              = Seq("3.15.6")
    val interpreterPane     = Seq("1.11.1")
    val jump3r              = Seq("1.0.5")
    val kollFlitz           = Seq("0.2.4")
    val linKernighan        = Seq("0.1.3")
    val log                 = Seq("0.1.1")
    val lucrePi             = Seq("1.11.0", "1.11.4")
    val negatum             = Seq("1.14.2")
    val patterns            = Seq("1.11.0")
    val pdflitz             = Seq("1.5.0")
    val pegDown             = Seq("1.6.0")
    val scalaColliderSwing  = Seq("2.9.2")
    val scissDSP            = Seq("2.2.6")
    val slf4j               = Seq("1.7.36")
    val submin              = Seq("0.3.6")
    val syntaxPane          = Seq("1.2.1")  // warning: 1.3.0 is buggy
    val treeTable           = Seq("1.6.3")
    val topology            = Seq("1.1.4")
    val webLaF              = Seq("1.2.14")
    val wolkenpumpe         = Seq("3.11.0")
  }
}

lazy val bdb = "bdb"

// ---- app packaging ----

lazy val appMainClass = Some("de.sciss.mellite.Mellite")
def appName           = baseName
def appNameL          = baseNameL

// ---- common ----

ThisBuild / organization  := "de.sciss"
ThisBuild / versionScheme := Some("pvp")

lazy val commonSettings = Seq(
  homepage           := Some(url(s"https://sciss.de/$baseNameL")),
  // note: license _name_ is printed in 'about' dialog
  licenses           := Seq("GNU Affero General Public License v3+" -> url("https://www.gnu.org/licenses/agpl-3.0.txt")),
  scalaVersion       := "2.13.11", // be careful that this matches Dotterweide, SoundProcesses, ScalaInterpreterPane!
  crossScalaVersions := Seq(/* "3.1.0", */ "2.13.11", "2.12.15"),
  scalacOptions     ++= Seq("-deprecation", "-unchecked", "-feature", "-encoding", "utf8"),
  scalacOptions ++= {
    // if (isDotty.value) Nil else
    Seq("-Xlint:-stars-align,_", "-Xsource:2.13")
  },
  scalacOptions ++= {
    if (loggingEnabled || isSnapshot.value) Nil else Seq("-Xelide-below", "INFO")
  },
  scalacOptions /* in (Compile, compile) */ ++= {
    val sq0 = if (scala.util.Properties.isJavaAtLeast("9")) List("-release", "8") else Nil // JDK >8 breaks API; skip scala-doc
    val sq1 = if (VersionNumber(scalaVersion.value).matchesSemVer(SemanticSelector(">=2.13"))) "-Wconf:cat=deprecation&msg=Widening conversion:s" :: sq0 else sq0 // nanny state defaults :-E
    sq1
  },
  scalaModuleInfo ~= (_.map(_.withOverrideScalaVersion(true))), // avoid bumping scala-compiler !
  javacOptions ++= Seq("-source", "1.8", "-target", "1.8"),
  // resolvers += "Typesafe Maven Repository" at "http://repo.typesafe.com/typesafe/maven-releases/", // https://stackoverflow.com/questions/23979577
  // resolvers += "Typesafe Simple Repository" at "http://repo.typesafe.com/typesafe/simple/maven-releases/", // https://stackoverflow.com/questions/20497271
  updateOptions := updateOptions.value.withLatestSnapshots(false),
  assembly / aggregate := false,
  // ---- publishing ----
  publishMavenStyle := true,
  publishTo := {
    Some(if (isSnapshot.value)
      "Sonatype Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"
    else
      "Sonatype Releases"  at "https://oss.sonatype.org/service/local/staging/deploy/maven2"
    )
  },
  Test / publishArtifact := false,
  pomIncludeRepository := { _ => false },
  developers := List(
    Developer(
      id    = "sciss",
      name  = "Hanns Holger Rutz",
      email = "contact@sciss.de",
      url   = url("https://www.sciss.de")
    )
  ),
  scmInfo := {
    val h = gitRepoHost
    val a = s"$gitRepoUser/$gitRepoName"
    Some(ScmInfo(url(s"https://$h/$a"), s"scm:git@$h:$a.git"))
  },
)

// cf. https://github.com/mgarin/weblaf#java-9
lazy val openJavaPkg0 = List(
  "java.base/java.lang",
  "java.base/java.lang.reflect",
  "java.base/java.net",
  "java.base/java.text",
  "java.base/java.util",
  "java.desktop/java.awt",
  "java.desktop/java.awt.font",
  "java.desktop/java.awt.geom",
  "java.desktop/java.beans",
  "java.desktop/javax.swing.plaf.basic",
  "java.desktop/javax.swing.plaf.synth",
  "java.desktop/javax.swing",
  "java.desktop/javax.swing.table",
  "java.desktop/javax.swing.text",
  "java.desktop/sun.awt",
  "java.desktop/sun.font",
  "java.desktop/sun.swing",
  "java.desktop/sun.swing.table",
)

lazy val openJavaPkg = {
  var res = openJavaPkg0
  if (isLinux)    res = "java.desktop/com.sun.java.swing.plaf.gtk" :: res
  if (isWindows)  res = "java.desktop/com.sun.java.swing.plaf.windows" :: res
  if (isMac)      res = "java.desktop/com.apple.laf" :: "java.desktop/com.apple.eawt" :: res
  res
}

lazy val openJavaOptions = openJavaPkg.map(pkg => s"--add-opens=$pkg=ALL-UNNAMED")

// ---- packaging ----

//////////////// universal (directory) installer
lazy val pkgUniversalSettings = Seq(
  executableScriptName /* in Universal */ := appNameL,
  // NOTE: doesn't work on Windows, where we have to
  // provide manual file `MELLITE_config.txt` instead!
// NOTE: This workaround for #70 is incompatible with Java 11...
// instead recommend to users of Linux with JDK 8 to create
// `~/.accessibility.properties`
//
//  Universal / javaOptions ++= Seq(
//    // -J params will be added as jvm parameters
//    // "-J-Xmx1024m",
//    // others will be added as app parameters
//    "-Djavax.accessibility.assistive_technologies=",  // work around for #70
//  ),
  // Since our class path is very very long,
  // we use instead the wild-card, supported
  // by Java 6+. In the packaged script this
  // results in something like `java -cp "../lib/*" ...`.
  // NOTE: `in Universal` does not work. It therefore
  // also affects debian package building :-/
  // We need this settings for Windows.
  scriptClasspath /* in Universal */ := Seq("*"),
  Linux / name        := appName,
  Linux / packageName := appNameL, // XXX TODO -- what was this for?
  // Universal / mainClass := appMainClass,
  Universal / maintainer := s"$authorName <$authorEMail>",
  Universal / target := (Compile / target).value,
  Universal / javaOptions ++= {
    if (scala.util.Properties.isJavaAtLeast("9"))  openJavaOptions.map(s => s"-J$s") else Nil
  },
//  Universal / javaOptions ++= Seq("-Djavax.net.debug=all", "-Dhttps.protocols=TLSv1.2,TLSv1.1,TLSv1"),
)

//////////////// debian installer
lazy val pkgDebianSettings = Seq(
  Debian / packageName        := appNameL,  // this is the installed package (e.g. in `apt remove <name>`).
  Debian / packageSummary     := appDescription,
  // Debian / mainClass          := appMainClass,
  Debian / maintainer         := s"$authorName <$authorEMail>",
  Debian / packageDescription :=
    """Mellite is a computer music environment,
      | a desktop application based on SoundProcesses.
      | It manages workspaces of musical objects, including
      | sound processes, timelines, code fragments, or
      | live improvisation sets.
      |""".stripMargin,
  // include all files in src/debian in the installed base directory
  Debian / linuxPackageMappings ++= {
    val n     = appNameL // (Debian / name).value.toLowerCase
    val dir   = (Debian / sourceDirectory).value / "debian"
    val f1    = (dir * "*").filter(_.isFile).get  // direct child files inside `debian` folder
    val f2    = ((dir / "doc") * "*").get
    //
    def readOnly(in: LinuxPackageMapping) =
      in.withUser ("root")
        .withGroup("root")
        .withPerms("0644")  // http://help.unc.edu/help/how-to-use-unix-and-linux-file-permissions/
    //
    val aux   = f1.map { fIn => packageMapping(fIn -> s"/usr/share/$n/${fIn.name}") }
    val doc   = f2.map { fIn => packageMapping(fIn -> s"/usr/share/doc/$n/${fIn.name}") }
    (aux ++ doc).map(readOnly)
  }
)

//////////////// fat-jar assembly
lazy val assemblySettings = Seq(
    assembly / mainClass             := appMainClass,
    assembly / target                := baseDirectory.value,
    assembly / assemblyJarName       := s"$baseName.jar",
    assembly / assemblyMergeStrategy := {
      case PathList("org", "xmlpull", _ @ _*)                   => MergeStrategy.first
      case PathList("org", "w3c", "dom", "events", _ @ _*)      => MergeStrategy.first // bloody Apache Batik
      case PathList("META-INF", "io.netty.versions.properties") => MergeStrategy.first
      case x =>
        val oldStrategy = (assembly / assemblyMergeStrategy).value
        oldStrategy(x)
    },
    packageOptions += ManifestAttributes(("Add-Opens", openJavaPkg.mkString(" ")))
  )

// ---- projects ----

lazy val root = project.withId(baseNameL).in(file("."))
  .aggregate(core, bundle, app, full)
  .dependsOn(core, bundle, app)
  .settings(commonSettings)
  .settings(
    name    := baseName,
    version := appVersion,
    Compile / packageBin / publishArtifact := false, // there are no binaries
    Compile / packageDoc / publishArtifact := false, // there are no javadocs
    Compile / packageSrc / publishArtifact := false, // there are no sources
    // packagedArtifacts := Map.empty
    autoScalaLibrary := false
  )

lazy val core = project.withId(s"$baseNameL-core").in(file("core"))
  .settings(commonSettings)
  .settings(
    name        := s"$baseName-core",
    version     := coreVersion,
    description := s"$baseName - core library",
    libraryDependencies ++= Seq(
      "de.sciss"          %% "asyncfile"                      % deps.common.asyncFile.head,          // file I/O
      "de.sciss"          %% "audiofile"                      % deps.common.audioFile.head,          // audio file I/O
      "de.sciss"          %% "audiowidgets-app"               % deps.common.audioWidgets.head,       // audio application widgets
      "de.sciss"          %% "audiowidgets-core"              % deps.common.audioWidgets.head,       // audio application widgets
      "de.sciss"          %% "audiowidgets-swing"             % deps.common.audioWidgets.head,       // audio application widgets
      "de.sciss"          %% "desktop-core"                   % deps.common.desktop.head,            // support for desktop applications
      "de.sciss"          %% "equal"                          % deps.common.equal.head,              // type-safe equals operator
      "de.sciss"          %% "fileutil"                       % deps.common.fileUtil.head,           // utility methods for file paths
      "de.sciss"          %% "lucre-base"                     % deps.common.lucre.head,              // transactional objects
      "de.sciss"          %% "lucre-confluent"                % deps.common.lucre.head,              // transactional objects
      "de.sciss"          %% "lucre-core"                     % deps.common.lucre.head,              // transactional objects
      "de.sciss"          %% "lucre-expr"                     % deps.common.lucre.head,              // transactional objects
      "de.sciss"          %% "lucre-swing"                    % deps.common.lucreSwing.head,         // reactive Swing components
      "de.sciss"          %% "lucre-synth"                    % deps.common.soundProcesses.head,     // computer-music framework
      "de.sciss"          %% "model"                          % deps.common.model.head,              // publisher-subscriber library
      "de.sciss"          %% "numbers"                        % deps.common.numbers.head,            // extension methods for numbers
      "de.sciss"          %% "processor"                      % deps.common.processor.head,          // futures with progress and cancel
      "de.sciss"          %% "raphael-icons"                  % deps.common.raphaelIcons.head,       // icon set
      "de.sciss"          %% "scalaosc"                       % deps.common.scalaOSC.head,           // open sound control
      "de.sciss"          %% "scalacollider"                  % deps.common.scalaCollider.head,      // realtime sound synthesis
      "de.sciss"          %% "scalacolliderugens-api"         % deps.common.scalaColliderUGens.head, // realtime sound synthesis
      "de.sciss"          %% "scalacolliderugens-core"        % deps.common.scalaColliderUGens.head, // realtime sound synthesis
      "de.sciss"          %% "scalacollider-if"               % deps.common.scalaColliderIf.head,    // realtime sound synthesis
      "de.sciss"          %% "serial"                         % deps.common.serial.head,             // serialization
      "de.sciss"          %% "sonogramoverview"               % deps.common.sonogram.head,           // sonogram component
      "de.sciss"          %% "soundprocesses-core"            % deps.common.soundProcesses.head,     // computer-music framework
      "de.sciss"          %% "soundprocesses-views"           % deps.common.soundProcesses.head,     // computer-music framework
      "de.sciss"          %% "span"                           % deps.common.span.head,               // time spans
      "de.sciss"          %% "swingplus"                      % deps.common.swingPlus.head,          // Swing extensions
      "org.rogach"        %% "scallop"                        % deps.common.scallop.head,            // command line option parsing
      "org.scala-lang.modules" %% "scala-swing"               % deps.common.scalaSwing.head,         // desktop UI kit
      "org.scala-stm"     %% "scala-stm"                      % deps.common.scalaSTM.head,           // software transactional memory
    ),
    mimaPreviousArtifacts := Set("de.sciss" %% s"$baseNameL-core" % mimaCoreVersion),
  )

lazy val appSettings = Seq(
  description         := appDescription,
  Compile / mainClass := appMainClass,
)

lazy val bundle = project.withId(s"$baseNameL-bundle").in(file("bundle"))
  .dependsOn(core)
  .settings(commonSettings)
  .settings(appSettings)
  .settings(
    name    := s"$baseName-bundle",
    version := bundleVersion,
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %%  "akka-stream"                   % deps.app.akka.head,
      "com.typesafe.akka" %%  "akka-stream-testkit"           % deps.app.akka.head,
      "de.sciss"          %% "asyncfile"                      % deps.common.asyncFile.head,          // file I/O
      "de.sciss"          %% "audiofile"                      % deps.common.audioFile.head,          // reading/writing audio files
      "de.sciss"          %% "audiowidgets-app"               % deps.common.audioWidgets.head,       // audio application widgets
      "de.sciss"          %% "audiowidgets-core"              % deps.common.audioWidgets.head,       // audio application widgets
      "de.sciss"          %% "audiowidgets-swing"             % deps.common.audioWidgets.head,       // audio application widgets
      "de.sciss"          %% "desktop-core"                   % deps.common.desktop.head,            // support for desktop applications
      "de.sciss"          %% "desktop-linux"                  % deps.common.desktop.head,            // support for desktop applications
      "de.sciss"          %% "dotterweide-core"               % deps.app.dotterweide.head,           // Code editor
      "de.sciss"          %% "dotterweide-doc-browser"        % deps.app.dotterweide.head,           // Code editor
      "de.sciss"          %% "dotterweide-scala"              % deps.app.dotterweide.head,           // Code editor
      "de.sciss"          %% "dotterweide-ui"                 % deps.app.dotterweide.head,           // Code editor
      "de.sciss"          %% "desktop-mac"                    % deps.common.desktop.head,            // support for desktop applications
      "de.sciss"          %% "equal"                          % deps.common.equal.head,              // type-safe equals
      "de.sciss"          %% "filecache-common"               % deps.app.fileCache.head,             // caching data to disk
      "de.sciss"          %% "fileutil"                       % deps.common.fileUtil.head,           // extension methods for files
      "de.sciss"          %% "fingertree"                     % deps.app.fingerTree.head,            // data structures
      "de.sciss"          %% "fscape-core"                    % deps.app.fscape.head,                // offline audio rendering
      "de.sciss"          %% "fscape-lucre"                   % deps.app.fscape.head,                // offline audio rendering
      "de.sciss"          %% "fscape-views"                   % deps.app.fscape.head,                // offline audio rendering
      "de.sciss"          %  "jump3r"                         % deps.app.jump3r.head,                // mp3 export
      "de.sciss"          %% "kollflitz"                      % deps.app.kollFlitz.head,             // more collections methods
      "de.sciss"          %% "linkernighantsp"                % deps.app.linKernighan.head,          // used by FScape
      "de.sciss"          %% "log"                            % deps.app.log.head,                   // logging
      "de.sciss"          %% "lucre-adjunct"                  % deps.common.lucre.head,              // object system
      "de.sciss"          %% "lucre-base"                     % deps.common.lucre.head,              // object system
      "de.sciss"          %% s"lucre-$bdb"                    % deps.common.lucre.head,              // object system (database backend)
      "de.sciss"          %% "lucre-confluent"                % deps.common.lucre.head,              // object system
      "de.sciss"          %% "lucre-core"                     % deps.common.lucre.head,              // object system
      "de.sciss"          %% "lucre-expr"                     % deps.common.lucre.head,              // object system
      "de.sciss"          %% "lucre-swing"                    % deps.common.lucreSwing.head,         // reactive Swing components
      "de.sciss"          %% "lucre-synth"                    % deps.common.soundProcesses.head,     // computer-music framework
      "de.sciss"          %% "model"                          % deps.common.model.head,              // non-txn MVC
      "de.sciss"          %% "negatum-core"                   % deps.app.negatum.head,               // genetic programming of sounds
      "de.sciss"          %% "negatum-views"                  % deps.app.negatum.head,               // genetic programming of sounds
      "de.sciss"          %% "numbers"                        % deps.common.numbers.head,            // extension methods for numbers
      "de.sciss"          %% "patterns-core"                  % deps.app.patterns.head,              // pattern sequences
      "de.sciss"          %% "patterns-lucre"                 % deps.app.patterns.head,              // pattern sequences
      "de.sciss"          %% "processor"                      % deps.common.processor.head,          // futures with progress and cancel
      "de.sciss"          %% "pdflitz"                        % deps.app.pdflitz.head,               // PDF export
      "de.sciss"          %% "raphael-icons"                  % deps.common.raphaelIcons.head,       // icon set
      "de.sciss"          %% "scalacollider"                  % deps.common.scalaCollider.head,      // realtime sound synthesis
      "de.sciss"          %% "scalacolliderugens-api"         % deps.common.scalaColliderUGens.head, // realtime sound synthesis
      "de.sciss"          %% "scalacolliderugens-core"        % deps.common.scalaColliderUGens.head, // realtime sound synthesis
      "de.sciss"          %% "scalacolliderugens-plugins"     % deps.common.scalaColliderUGens.head, // realtime sound synthesis
      "de.sciss"          %  "scalacolliderugens-spec"        % deps.common.scalaColliderUGens.head, // realtime sound synthesis
      "de.sciss"          %% "scalacolliderswing-core"        % deps.app.scalaColliderSwing.head,    // UI methods for scala-collider
      "de.sciss"          %% "scalainterpreterpane"           % deps.app.interpreterPane.head,       // REPL
      "de.sciss"          %% "scalafreesound-lucre"           % deps.app.freesound.head,             // Freesound support
      "de.sciss"          %% "scalafreesound-views"           % deps.app.freesound.head,             // Freesound support
      "de.sciss"          %% "scalaosc"                       % deps.common.scalaOSC.head,           // open sound control
      "de.sciss"          %% "scissdsp"                       % deps.app.scissDSP.head,              // offline signal processing
      "de.sciss"          %% "serial"                         % deps.common.serial.head,             // serialization
      "de.sciss"          %% "sonogramoverview"               % deps.common.sonogram.head,           // sonogram component
      "de.sciss"          %% "soundprocesses-compiler"        % deps.common.soundProcesses.head,     // computer-music framework
      "de.sciss"          %% "soundprocesses-core"            % deps.common.soundProcesses.head,     // computer-music framework
      "de.sciss"          %% "soundprocesses-views"           % deps.common.soundProcesses.head,     // computer-music framework
      "de.sciss"          %% "span"                           % deps.common.span.head,               // time spans
      "de.sciss"          %  "submin"                         % deps.app.submin.head,                // dark skin
      "de.sciss"          %  "syntaxpane"                     % deps.app.syntaxPane.head,            // code editor
      "de.sciss"          %% "swingplus"                      % deps.common.swingPlus.head,          // Swing extensions
      "de.sciss"          %% "topology"                       % deps.app.topology.head,              // graph sorting
      "de.sciss"          %  "treetable-java"                 % deps.app.treeTable.head,             // widget
      "de.sciss"          %% "treetable-scala"                % deps.app.treeTable.head,             // widget
      "com.weblookandfeel" % "weblaf-core"                    % deps.app.webLaF.head,
      "com.weblookandfeel" % "weblaf-ui"                      % deps.app.webLaF.head,
      "de.sciss"          %% "wolkenpumpe-basic"              % deps.app.wolkenpumpe.head,           // live improvisation
      "de.sciss"          %% "wolkenpumpe-core"               % deps.app.wolkenpumpe.head,           // live improvisation
      "net.harawata"      %  "appdirs"                        % deps.app.appDirs.head,               // finding cache directory
      "org.pegdown"       %  "pegdown"                        % deps.app.pegDown.head,               // Markdown renderer
      "org.rogach"        %% "scallop"                        % deps.common.scallop.head,            // command line option parsing
      "org.scala-lang.modules" %% "scala-swing"               % deps.common.scalaSwing.head,         // desktop UI kit
      "org.scala-stm"     %% "scala-stm"                      % deps.common.scalaSTM.head,           // software transactional memory
      "org.slf4j"         %  "slf4j-api"                      % deps.app.slf4j.head,                 // logging (used by weblaf)
      "org.slf4j"         %  "slf4j-simple"                   % deps.app.slf4j.head,                 // logging (used by weblaf)
      "org.scala-lang"    %  "scala-compiler"                 % scalaVersion.value,             // embedded compiler
    ),
    libraryDependencies += {
      val x = "de.sciss" %% "lucre-pi" % deps.app.lucrePi.head  // Raspberry Pi support
      if (archSuffix == "x64") x exclude ("javax.xml.bind", "jaxb-api") else x // fix while building full binaries not on Pi
    },
    mimaPreviousArtifacts := Set("de.sciss" %% s"$baseNameL-bundle" % mimaBundleVersion),
  )

lazy val app = project.withId(s"$baseNameL-app").in(file("app"))
  .enablePlugins(BuildInfoPlugin)
  .enablePlugins(JavaAppPackaging, DebianPlugin)
  .dependsOn(bundle)
  .settings(commonSettings)
  .settings(pkgUniversalSettings)
  .settings(pkgDebianSettings)
  .settings(useNativeZip) // cf. https://github.com/sbt/sbt-native-packager/issues/334
  .settings(assemblySettings)
  .settings(appSettings)
  .settings(
    name    := s"$baseName-app",
    version := appVersion,
    // resolvers += "Oracle Repository" at "http://download.oracle.com/maven", // required for sleepycat
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %%  "akka-stream"                   % deps.app.akka.last,
      "com.typesafe.akka" %%  "akka-stream-testkit"           % deps.app.akka.last,
      "de.sciss"          %% "asyncfile"                      % deps.common.asyncFile.last,          // file I/O
      "de.sciss"          %% "audiofile"                      % deps.common.audioFile.last,          // reading/writing audio files
      "de.sciss"          %% "audiowidgets-app"               % deps.common.audioWidgets.last,       // audio application widgets
      "de.sciss"          %% "audiowidgets-core"              % deps.common.audioWidgets.last,       // audio application widgets
      "de.sciss"          %% "audiowidgets-swing"             % deps.common.audioWidgets.last,       // audio application widgets
      "de.sciss"          %% "desktop-core"                   % deps.common.desktop.last,            // support for desktop applications
      "de.sciss"          %% "desktop-linux"                  % deps.common.desktop.last,            // support for desktop applications
      "de.sciss"          %% "dotterweide-core"               % deps.app.dotterweide.last,           // Code editor
      "de.sciss"          %% "dotterweide-doc-browser"        % deps.app.dotterweide.last,           // Code editor
      "de.sciss"          %% "dotterweide-scala"              % deps.app.dotterweide.last,           // Code editor
      "de.sciss"          %% "dotterweide-ui"                 % deps.app.dotterweide.last,           // Code editor
      "de.sciss"          %% "desktop-mac"                    % deps.common.desktop.last,            // support for desktop applications
      "de.sciss"          %% "equal"                          % deps.common.equal.last,              // type-safe equals
      "de.sciss"          %% "filecache-common"               % deps.app.fileCache.last,             // caching data to disk
      "de.sciss"          %% "fileutil"                       % deps.common.fileUtil.last,           // extension methods for files
      "de.sciss"          %% "fingertree"                     % deps.app.fingerTree.last,            // data structures
      "de.sciss"          %% "fscape-core"                    % deps.app.fscape.last,                // offline audio rendering
      "de.sciss"          %% "fscape-lucre"                   % deps.app.fscape.last,                // offline audio rendering
      "de.sciss"          %% "fscape-views"                   % deps.app.fscape.last,                // offline audio rendering
      "de.sciss"          %  "jump3r"                         % deps.app.jump3r.last,                // mp3 export
      "de.sciss"          %% "kollflitz"                      % deps.app.kollFlitz.last,             // more collections methods
      "de.sciss"          %% "linkernighantsp"                % deps.app.linKernighan.last,          // used by FScape
      "de.sciss"          %% "log"                            % deps.app.log.last,                   // logging
      "de.sciss"          %% "lucre-adjunct"                  % deps.common.lucre.last,              // object system
      "de.sciss"          %% "lucre-base"                     % deps.common.lucre.last,              // object system
      "de.sciss"          %% s"lucre-$bdb"                    % deps.common.lucre.last,              // object system (database backend)
      "de.sciss"          %% "lucre-confluent"                % deps.common.lucre.last,              // object system
      "de.sciss"          %% "lucre-core"                     % deps.common.lucre.last,              // object system
      "de.sciss"          %% "lucre-expr"                     % deps.common.lucre.last,              // object system
      "de.sciss"          %% "lucre-swing"                    % deps.common.lucreSwing.last,         // reactive Swing components
      "de.sciss"          %% "lucre-synth"                    % deps.common.soundProcesses.last,     // computer-music framework
      "de.sciss"          %% "model"                          % deps.common.model.last,              // non-txn MVC
      "de.sciss"          %% "negatum-core"                   % deps.app.negatum.last,               // genetic programming of sounds
      "de.sciss"          %% "negatum-views"                  % deps.app.negatum.last,               // genetic programming of sounds
      "de.sciss"          %% "numbers"                        % deps.common.numbers.last,            // extension methods for numbers
      "de.sciss"          %% "patterns-core"                  % deps.app.patterns.last,              // pattern sequences
      "de.sciss"          %% "patterns-lucre"                 % deps.app.patterns.last,              // pattern sequences
      "de.sciss"          %% "processor"                      % deps.common.processor.last,          // futures with progress and cancel
      "de.sciss"          %% "pdflitz"                        % deps.app.pdflitz.last,               // PDF export
      "de.sciss"          %% "raphael-icons"                  % deps.common.raphaelIcons.last,       // icon set
      "de.sciss"          %% "scalacollider"                  % deps.common.scalaCollider.last,      // realtime sound synthesis
      "de.sciss"          %% "scalacolliderugens-api"         % deps.common.scalaColliderUGens.last, // realtime sound synthesis
      "de.sciss"          %% "scalacolliderugens-core"        % deps.common.scalaColliderUGens.last, // realtime sound synthesis
      "de.sciss"          %% "scalacolliderugens-plugins"     % deps.common.scalaColliderUGens.last, // realtime sound synthesis
      "de.sciss"          %  "scalacolliderugens-spec"        % deps.common.scalaColliderUGens.last, // realtime sound synthesis
      "de.sciss"          %% "scalacolliderswing-core"        % deps.app.scalaColliderSwing.last,    // UI methods for scala-collider
      "de.sciss"          %% "scalainterpreterpane"           % deps.app.interpreterPane.last,       // REPL
      "de.sciss"          %% "scalafreesound-lucre"           % deps.app.freesound.last,             // Freesound support
      "de.sciss"          %% "scalafreesound-views"           % deps.app.freesound.last,             // Freesound support
      "de.sciss"          %% "scalaosc"                       % deps.common.scalaOSC.last,           // open sound control
      "de.sciss"          %% "scissdsp"                       % deps.app.scissDSP.last,              // offline signal processing
      "de.sciss"          %% "serial"                         % deps.common.serial.last,             // serialization
      "de.sciss"          %% "sonogramoverview"               % deps.common.sonogram.last,           // sonogram component
      "de.sciss"          %% "soundprocesses-compiler"        % deps.common.soundProcesses.last,     // computer-music framework
      "de.sciss"          %% "soundprocesses-core"            % deps.common.soundProcesses.last,     // computer-music framework
      "de.sciss"          %% "soundprocesses-views"           % deps.common.soundProcesses.last,     // computer-music framework
      "de.sciss"          %% "span"                           % deps.common.span.last,               // time spans
      "de.sciss"          %  "submin"                         % deps.app.submin.last,                // dark skin
      "de.sciss"          %  "syntaxpane"                     % deps.app.syntaxPane.last,            // code editor
      "de.sciss"          %% "swingplus"                      % deps.common.swingPlus.last,          // Swing extensions
      "de.sciss"          %% "topology"                       % deps.app.topology.last,              // graph sorting
      "de.sciss"          %  "treetable-java"                 % deps.app.treeTable.last,             // widget
      "de.sciss"          %% "treetable-scala"                % deps.app.treeTable.last,             // widget
      "com.weblookandfeel" % "weblaf-core"                    % deps.app.webLaF.last,
      "com.weblookandfeel" % "weblaf-ui"                      % deps.app.webLaF.last,
      "de.sciss"          %% "wolkenpumpe-basic"              % deps.app.wolkenpumpe.last,           // live improvisation
      "de.sciss"          %% "wolkenpumpe-core"               % deps.app.wolkenpumpe.last,           // live improvisation
      "net.harawata"      %  "appdirs"                        % deps.app.appDirs.last,               // finding cache directory
      "org.pegdown"       %  "pegdown"                        % deps.app.pegDown.last,               // Markdown renderer
      "org.rogach"        %% "scallop"                        % deps.common.scallop.last,            // command line option parsing
      "org.scala-lang.modules" %% "scala-swing"               % deps.common.scalaSwing.last,         // desktop UI kit
      "org.scala-stm"     %% "scala-stm"                      % deps.common.scalaSTM.last,           // software transactional memory
      "org.slf4j"         %  "slf4j-api"                      % deps.app.slf4j.last,                 // logging (used by weblaf)
      "org.slf4j"         %  "slf4j-simple"                   % deps.app.slf4j.last,                 // logging (used by weblaf)
      "org.scala-lang"    %  "scala-compiler"                 % scalaVersion.value,             // embedded compiler
    ),
    libraryDependencies += {
      val x = "de.sciss" %% "lucre-pi" % deps.app.lucrePi.last  // Raspberry Pi support
      if (archSuffix == "x64") x exclude ("javax.xml.bind", "jaxb-api") else x // fix while building full binaries not on Pi
    },
    mimaPreviousArtifacts := Set("de.sciss" %% s"$baseNameL-app" % mimaBundleVersion),
    console / initialCommands :=
      """import de.sciss.mellite._""".stripMargin,
    run / fork := true,  // required for shutdown hook, and also the scheduled thread pool, it seems
    run / javaOptions ++= openJavaOptions,
    // ---- build-info ----
    buildInfoKeys := Seq("name" -> baseName /* name */, organization, version, scalaVersion, description,
      BuildInfoKey.map(homepage) { case (k, opt) => k -> opt.get },
      BuildInfoKey.map(licenses) { case (_, Seq( (lic, _) )) => "license" -> lic },
      "issueTracker" -> gitIssueTracker,
    ),
    buildInfoOptions += BuildInfoOption.BuildTime,
    buildInfoPackage := "de.sciss.mellite",
    // ---- license report ----
    licenseReportTypes    := Seq(Csv),
    licenseConfigurations := Set(Compile.name),
    updateLicenses := {
      val regular = updateLicenses.value
      val fontsLic = DepLicense(
        DepModuleInfo("io.github.dejavu-fonts", "dejavu-fonts", deps.app.dejaVuFonts.last),
        LicenseInfo(LicenseCategory("OFL"), name = "DejaVu Fonts License",
          url = "https://dejavu-fonts.github.io/License.html"
        ),
        configs = Set(Compile.name)
      )
      regular.copy(licenses = regular.licenses :+ fontsLic)
    },
    licenseReportDir := (Compile / sourceDirectory).value / "resources" / "de" / "sciss" / "mellite",
    // ---- packaging ----
    Universal / packageName := s"${appNameL}_${version.value}_all",
    Debian / name                      := appNameL,  // this is used for .deb file-name; NOT appName,
    Debian / debianPackageDependencies ++= Seq("java11-runtime"),
    Debian / debianPackageRecommends   ++= Seq("openjfx"), // you could run without, just the API browser won't work
    // ---- publishing ----
    pomPostProcess := addChanges,
  )

lazy val osName = sys.props("os.name")
def isLinux     = osName.startsWith("Linux")
def isMac       = osName.startsWith("Mac")
def isWindows   = osName.startsWith("Windows")

// Determine OS version of JavaFX binaries
lazy val jfxClassifier =
  if (isLinux) "linux" else if (isMac) "mac" else if (isWindows) "win" else
    throw new Exception(s"Unknown platform ($osName)!")

def jfxDep(name: String): ModuleID =
  "org.openjfx" % s"javafx-$name" % "11.0.2" classifier jfxClassifier

def archSuffix: String =
  sys.props("os.arch") match {
    case "i386"  | "x86_32" => "x32"
    case "amd64" | "x86_64" => "x64"
    case other              => other
  }

lazy val addChanges: xml.Node => xml.Node = {
  case root: xml.Elem =>
    val changeNodes: Seq[xml.Node] = changeLog.map(t => <mllt.change>{t}</mllt.change>)
    val newChildren = root.child.map {
      case e: xml.Elem if e.label == "properties" => e.copy(child = e.child ++ changeNodes)
      case other => other
    }
    root.copy(child = newChildren)
}

lazy val full = project.withId(s"$baseNameL-full").in(file("full"))
  .dependsOn(app)
  .enablePlugins(JavaAppPackaging, DebianPlugin, JlinkPlugin)
  .settings(commonSettings)
  .settings(pkgUniversalSettings)
  .settings(pkgDebianSettings)
  // disabled so we don't need to install zip.exe on wine:
  // .settings(useNativeZip) // cf. https://github.com/sbt/sbt-native-packager/issues/334
  .settings(assemblySettings) // do we need this here?
  .settings(appSettings)
  .settings(
    name := s"$baseName-full",
    version := appVersion,
    jlinkIgnoreMissingDependency := JlinkIgnore.everything, // temporary for testing
    jlinkModules ++= Seq(
      "jdk.unsupported", // needed for Akka
      "java.management",
      "jdk.crypto.cryptoki",  // needed for freesound
    ),
    libraryDependencies ++= Seq("base", "swing", "controls", "graphics", "media", "web").map(jfxDep),
    Universal / packageName := s"${appNameL}-full_${version.value}_${jfxClassifier}_$archSuffix",
    Debian / name                := s"$appNameL-full",  // this is used for .deb file-name; NOT appName,
    Debian / packageArchitecture := sys.props("os.arch"), // archSuffix,
  )
