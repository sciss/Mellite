/*
 *  SonogramManager.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite

import java.io.File

import de.sciss.dsp.{ConstQ, Threading}
import de.sciss.sonogram
import de.sciss.sonogram.Overview.Palette

import scala.concurrent.ExecutionContext

object SonogramManager {
  private lazy val _instance = {
    val cfg               = sonogram.OverviewManager.Config()
    val folder            = new File(Mellite.cacheDir, "waveforms")
    folder.mkdirs()
    val sizeLimit         = 2L << 10 << 10 << 100  // 20 GB
    cfg.caching           = Some(sonogram.OverviewManager.Caching(folder, sizeLimit))
    // currently a problem with JTransforms
    // cfg.executionContext  = ExecutionContext.fromExecutorService(Executors.newSingleThreadExecutor())
    // cfg.executionContext  = ExecutionContext.fromExecutorService(Executors.newFixedThreadPool(2))
    sonogram.OverviewManager(cfg)
  }

  def instance: sonogram.OverviewManager = _instance

  private lazy val _palette: Palette =
    if (GUI.isDarkSkin) Palette.Intensity else Palette.reverse(Palette.Intensity)

  def acquire(file: File): sonogram.Overview = {
    val cq    = ConstQ.Config()
    // XXX TODO should be user configurable
    cq.bandsPerOct  = 24 // 18
    cq.maxTimeRes   = 12.0 // 18
//    cq.bandsPerOct  = 18 * 4
//    cq.maxTimeRes   = 18 / 4.0f
    cq.threading    = Threading.Single
    val job   = sonogram.OverviewManager.Job(file, cq)
    val res   = _instance.acquire(job)
    res.palette = _palette
    res
  }
  def release(overview: sonogram.Overview): Unit = _instance.release(overview)

  implicit def executionContext: ExecutionContext = _instance.config.executionContext
}