/*
 *  FolderFrame.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite

import de.sciss.lucre.expr.CellView
import de.sciss.lucre.synth.Txn
import de.sciss.lucre.{Folder, Txn => LTxn}
import de.sciss.mellite.impl.document.{FolderFrameImpl => Impl}

import scala.swing.{Component, SequentialContainer}

object FolderFrame extends WorkspaceWindow.Key {
  /** Creates a new frame for a folder view.
    *
    * @param name             optional window name
    * @param isWorkspaceRoot  if `true`, closes the workspace when the window closes; if `false` does nothing
    *                         upon closing the window
    */
  def apply[T <: Txn[T]](name: CellView[T, String], isWorkspaceRoot: Boolean)
                        (implicit tx: T, handler: UniverseHandler[T]): FolderFrame[T] = {
    val folder = handler.universe.workspace.root
    Impl(name = name, folder = folder, isWorkspaceRoot = isWorkspaceRoot)
  }

  def apply[T <: Txn[T]](name: CellView[T, String], folder: Folder[T])
                        (implicit tx: T, handler: UniverseHandler[T]): FolderFrame[T] = {
    Impl(name = name, folder = folder, isWorkspaceRoot = false)
  }

  type Repr[T <: LTxn[T]] = FolderFrame[T]
}

trait FolderFrame[T <: LTxn[T]] extends WorkspaceWindow[T] {
  type Repr[~ <: LTxn[~]] = FolderFrame[~]

  override def key: Key = FolderFrame

  override def view: FolderEditorView[T]

  def folderView: FolderView[T]

  def bottomComponent: Component with SequentialContainer
}