/*
 *  NuagesEditorFrame.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite

import de.sciss.lucre.synth.Txn
import de.sciss.lucre.{Txn => LTxn}
import de.sciss.mellite.impl.document.{NuagesEditorFrameImpl => Impl}
import de.sciss.nuages.Nuages

object NuagesEditorFrame extends WorkspaceWindow.Key {
  def apply[T <: Txn[T]](obj: Nuages[T])(implicit tx: T, handler: UniverseHandler[T]): NuagesEditorFrame[T] =
    Impl[T](obj)

  type Repr[T <: LTxn[T]] = NuagesEditorFrame[T]
}
trait NuagesEditorFrame[T <: LTxn[T]] extends WorkspaceWindow[T] {
  type Repr[~ <: LTxn[~]] = NuagesEditorFrame[~]

  override def view: NuagesEditorView[T]
}