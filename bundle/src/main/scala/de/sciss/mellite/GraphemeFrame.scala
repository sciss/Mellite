/*
 *  GraphemeFrame.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite

import de.sciss.lucre.synth.Txn
import de.sciss.lucre.{Txn => LTxn}
import de.sciss.mellite.impl.grapheme.{GraphemeFrameImpl => Impl}
import de.sciss.proc.Grapheme

object GraphemeFrame extends WorkspaceWindow.Key {
  def apply[T <: Txn[T]](obj: Grapheme[T])
                        (implicit tx: T, handler: UniverseHandler[T]): GraphemeFrame[T] =
    Impl(obj)

  type Repr[T <: LTxn[T]] = GraphemeFrame[T]
}
trait GraphemeFrame[T <: LTxn[T]] extends WorkspaceWindow[T] {
  type Repr[~ <: LTxn[~]] = GraphemeFrame[~]

  override def key: Key = GraphemeFrame

  override def view: GraphemeView[T]
}