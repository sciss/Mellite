/*
 *  AttrMapFrame.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite

import de.sciss.lucre.synth.Txn
import de.sciss.lucre.{Obj, Txn => LTxn}
import de.sciss.mellite.impl.document.{AttrMapFrameImpl => Impl}

object AttrMapFrame extends WorkspaceWindow.Key {
  def apply[T <: Txn[T]](obj: Obj[T])(implicit tx: T, handler: UniverseHandler[T]): AttrMapFrame[T] =
    Impl(obj)

  type Repr[T <: LTxn[T]] = AttrMapFrame[T]
}
trait AttrMapFrame[T <: LTxn[T]] extends WorkspaceWindow[T] {
  type Repr[~ <: LTxn[~]] = AttrMapFrame[~]

  override def key: Key = AttrMapFrame

  def contents: AttrMapView[T]  // XXX TODO - should really be `view`
}