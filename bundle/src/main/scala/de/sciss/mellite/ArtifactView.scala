/*
 *  ArtifactView.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite

import de.sciss.desktop.FileDialog
import de.sciss.lucre.edit.UndoManager
import de.sciss.lucre.swing.View
import de.sciss.lucre.synth.Txn
import de.sciss.lucre.{Artifact, Txn => LTxn}
import de.sciss.mellite.impl.artifact.{ArtifactViewImpl => Impl}

object ArtifactView {
  def apply[T <: Txn[T]](obj: Artifact[T], mode: Boolean, initMode: FileDialog.Mode)
                        (implicit tx: T, handler: UniverseHandler[T],
                                           undo: UndoManager[T]): ArtifactView[T] =
    Impl(obj, mode = mode, initMode = initMode)
}
trait ArtifactView[T <: LTxn[T]] extends UniverseObjView[T] with View.Editable[T] {
  override def obj(implicit tx: T): Artifact[T]
}