/*
 *  WidgetFrame.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite

import de.sciss.lucre.swing.View
import de.sciss.lucre.synth.Txn
import de.sciss.lucre.{Txn => LTxn}
import de.sciss.mellite.impl.widget.WidgetFrameImpl
import de.sciss.proc.Widget

import scala.collection.immutable.{Seq => ISeq}

object WidgetEditorFrame extends WorkspaceWindow.Key {
  def apply[T <: Txn[T]](obj: Widget[T], bottom: ISeq[View[T]] = Nil)
                        (implicit tx: T, handler: UniverseHandler[T]): WidgetEditorFrame[T] =
    WidgetFrameImpl.editor(obj, bottom = bottom)

  type Repr[T <: LTxn[T]] = WidgetEditorFrame[T]
}

trait WidgetEditorFrame[T <: LTxn[T]] extends WorkspaceWindow[T] {
  type Repr[~ <: LTxn[~]] = WidgetEditorFrame[~]

  override def key: Key = WidgetEditorFrame

  override def view: WidgetEditorView[T]
}

object WidgetRenderFrame extends WorkspaceWindow.Key {
  def apply[T <: Txn[T]](obj: Widget[T])(implicit tx: T, handler: UniverseHandler[T]): WidgetRenderFrame[T] =
    WidgetFrameImpl.render(obj)

  type Repr[T <: LTxn[T]] = WidgetRenderFrame[T]
}

trait WidgetRenderFrame[T <: LTxn[T]] extends WorkspaceWindow[T] {
  type Repr[~ <: LTxn[~]] = WidgetRenderFrame[~]

  override def view: WidgetRenderView[T]
}