/*
 *  NuagesEditorFrameImpl.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite.impl.document

import de.sciss.lucre.edit.UndoManager
import de.sciss.lucre.expr.CellView
import de.sciss.lucre.synth.Txn
import de.sciss.mellite.impl.WorkspaceWindowImpl
import de.sciss.mellite.{NuagesEditorFrame, NuagesEditorView, UniverseHandler}
import de.sciss.nuages.Nuages

object NuagesEditorFrameImpl {
  def apply[T <: Txn[T]](obj: Nuages[T])
                        (implicit tx: T, handler: UniverseHandler[T]): NuagesEditorFrame[T] =
    handler(obj, NuagesEditorFrame)(newInstance(obj))

  private def newInstance[T <: Txn[T]](obj: Nuages[T])
                        (implicit tx: T, handler: UniverseHandler[T]): NuagesEditorFrame[T] = {
    implicit val undo: UndoManager[T] = UndoManager()
    val view          = NuagesEditorView(obj)
    val name          = CellView.name(obj)
    val res           = new FrameImpl[T](view)
    res.init().setTitle(name)
    res
  }

  private final class FrameImpl[T <: Txn[T]](val view: NuagesEditorView[T])
                                            (implicit val handler: UniverseHandler[T])
    extends NuagesEditorFrame[T] with WorkspaceWindowImpl[T] {

    override def key: Key = NuagesEditorFrame

    override def newWindow()(implicit tx: T): NuagesEditorFrame[T] =
      newInstance(view.obj)

    override protected def initGUI(): Unit = {
      FolderFrameImpl.addDuplicateAction(this, view.actionDuplicate) // XXX TODO -- all hackish
    }
  }
}