/*
 *  PatternObjView.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite.impl.patterns

import de.sciss.icons.raphael
import de.sciss.lucre.edit.UndoManager
import de.sciss.lucre.swing._
import de.sciss.lucre.swing.edit.EditVar
import de.sciss.lucre.synth.Txn
import de.sciss.lucre.{Ident, Obj, Plain, Source, SpanLikeObj, Txn => LTxn}
import de.sciss.mellite.impl.code.CodeFrameImpl
import de.sciss.mellite.impl.objview.ObjListViewImpl.NonEditable
import de.sciss.mellite.impl.objview.{NoArgsListObjViewFactory, ObjListViewImpl, ObjViewImpl}
import de.sciss.mellite.impl.timeline.ObjTimelineViewBasicImpl
import de.sciss.mellite.{CodeFrame, CodeView, GUI, ObjListView, ObjTimelineView, ObjView, RunnerToggleButton, Shapes, UniverseHandler}
import de.sciss.patterns
import de.sciss.patterns.graph.Pat
import de.sciss.proc.Implicits._
import de.sciss.proc.{Code, Pattern}

import javax.swing.Icon
import scala.swing.Button

object PatternObjView extends NoArgsListObjViewFactory with ObjTimelineView.Factory {
  type E[~ <: LTxn[~]] = Pattern[~]
  val icon          : Icon      = ObjViewImpl.raphaelIcon(Shapes.Pattern)
  val prefix        : String    = "Pattern"
  def humanName     : String    = prefix
  def tpe           : Obj.Type  = Pattern
  def category      : String    = ObjView.categComposition

  def mkListView[T <: Txn[T]](obj: Pattern[T])(implicit tx: T): PatternObjView[T] with ObjListView[T] = {
//    val vr = Pattern.Var.unapply(obj).getOrElse {
//      val _vr = Pattern.newVar[T](obj)
//      _vr
//    }
    new ListImpl(tx.newHandle(obj)).initAttrs(obj)
  }

  def mkTimelineView[T <: Txn[T]](id: Ident[T], span: SpanLikeObj[T], obj: Pattern[T],
                                  context: ObjTimelineView.Context[T])(implicit tx: T): ObjTimelineView[T] = {
    val res = new TimelineImpl[T](tx.newHandle(obj)).initAttrs(id, span, obj)
    res
  }

  private final class ListImpl[T <: Txn[T]](val objH: Source[T, Pattern[T]])
    extends Impl[T]

  private final class TimelineImpl[T <: Txn[T]](val objH : Source[T, Pattern[T]])
    extends Impl[T] with ObjTimelineViewBasicImpl[T]

  def makeObj[T <: Txn[T]](name: String)(implicit tx: T): List[Obj[T]] = {
    val obj  = Pattern.newVar[T](Pattern.empty[T])
    if (name.nonEmpty) obj.name = name
    obj :: Nil
  }

  private abstract class Impl[T <: Txn[T]]
    extends PatternObjView[T]
      with ObjListView /* .Int */[T]
      with ObjViewImpl.Impl[T]
      with ObjListViewImpl.EmptyRenderer[T]
      with NonEditable[T]
      /* with NonViewable[T] */ {

    override def objH: Source[T, Pattern[T]]

    override def obj(implicit tx: T): Pattern[T] = objH()

    type E[~ <: LTxn[~]] = Pattern[~]

    final def factory: ObjView.Factory = PatternObjView

    final def isViewable = true

    // currently this just opens a code editor. in the future we should
    // add a scans map editor, and a convenience button for the attributes
    override def openView(parent: Option[Window[T]])
                (implicit tx: T, handler: UniverseHandler[T]): Option[Window[T]] = {
      Pattern.Var.unapply(obj).map { vr =>
        import de.sciss.mellite.Mellite.compiler
        val frame = codeFrame(vr)
        frame
      }
    }

    // ---- adapter for editing an Pattern's source ----
  }

  private def codeFrame[T <: Txn[T]](obj: Pattern.Var[T])
                                    (implicit tx: T, universeHandler: UniverseHandler[T],
                                     compiler: Code.Compiler): CodeFrame[T] =
    universeHandler(obj, CodeFrame) {
      import universeHandler.universe

      val codeObj     = CodeFrameImpl.mkSource(obj = obj, codeTpe = Pattern.Code)()
      val swapObjOpt  = codeObj.attr.$[Code.Obj](CodeView.attrSwap)
      val built0      = swapObjOpt.isEmpty

      val objH        = tx.newHandle(obj)
      val code0       = (swapObjOpt getOrElse codeObj).value match {
        case cs: Pattern.Code => cs
        case other => sys.error(s"Pattern source code does not produce patterns.Graph: ${other.tpe.humanName}")
      }

      val handler = new CodeView.Handler[T, Unit, Pat[_]] {
        override def in(): Unit = ()

        override def save(in: Unit, out: Pat[_])(implicit tx: T, undo: UndoManager[T]): Unit = {
          val obj = objH()
          EditVar.exprUndo[T, Pat[_], Pattern]("Change Pattern Graph", obj, Pattern.newConst[T](out))
        }

        def dispose()(implicit tx: T): Unit = ()
      }

      val viewEval = View.wrap[T, Button] {
        val actionEval = new swing.Action("Evaluate") { self =>
          import universe.cursor
          def apply(): Unit = {
            implicit val ctx: patterns.Context[Plain] = patterns.Context()
            val n = 60
            val res0 = cursor.step { implicit tx =>
              val obj = objH()
              val g   = obj.value
              val st  = g.expand
              st.toIterator.take(n).toList
            }
            val abbr  = res0.lengthCompare(n) == 0
            val res   = if (abbr) res0.init else res0
            println(res.mkString("[", ", ", if (abbr) " ...]" else "]"))
          }
        }
        GUI.toolButton(actionEval, raphael.Shapes.Quote)
      }

      val viewPower = RunnerToggleButton(obj)

      val bottom = viewEval :: viewPower :: Nil

      implicit val undo: UndoManager[T] = UndoManager()
      CodeFrameImpl.newInstance(obj, objH, codeObj, code0,
        handler       = Some(handler),
        bottom        = bottom,
        rightViewOpt  = None,
        built0        = built0,
        canBounce     = true
      )
    }
}
trait PatternObjView[T <: LTxn[T]] extends ObjView[T] {
  type Repr = Pattern[T]
}