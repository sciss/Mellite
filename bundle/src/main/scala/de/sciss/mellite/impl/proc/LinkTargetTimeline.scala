/*
 *  LinkTargetTimeline.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite.impl.proc

import de.sciss.lucre.edit.{EditTimeline, UndoManager}
import de.sciss.lucre.synth.Txn
import de.sciss.lucre.{Obj, Source, SpanLikeObj}
import de.sciss.mellite.impl.proc.ProcObjView.LinkTarget

final class LinkTargetTimeline[T <: Txn[T]](val attr: InputAttrTimeline[T],
                                            spanH: Source[T, SpanLikeObj[T]],
                                            objH : Source[T, Obj[T]])
  extends LinkTarget[T] {

  override def toString: String = s"LinkTargetTimeline($attr)@${hashCode.toHexString}"

  override def remove()(implicit tx: T, undoManager: UndoManager[T]): Boolean = {
    val tl = attr.timeline
    tl.modifiableOption.exists { tlMod =>
      val span = spanH()
      val obj  = objH()
      EditTimeline.removeUndo(/*"Remove Proc",*/ tlMod, span, obj)
      true
    }
  }
}
