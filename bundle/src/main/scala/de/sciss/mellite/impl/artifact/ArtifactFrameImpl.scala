/*
 *  ArtifactFrameImpl.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite.impl.artifact

import de.sciss.desktop.FileDialog
import de.sciss.lucre.Artifact
import de.sciss.lucre.edit.UndoManager
import de.sciss.lucre.expr.CellView
import de.sciss.lucre.synth.Txn
import de.sciss.mellite.impl.WorkspaceWindowImpl
import de.sciss.mellite.impl.objview.ArtifactObjView.humanName
import de.sciss.mellite.{ArtifactFrame, ArtifactView, UniverseHandler}

object ArtifactFrameImpl {
  def apply[T <: Txn[T]](obj: Artifact[T], mode: Boolean, initMode: FileDialog.Mode)
                        (implicit tx: T, handler: UniverseHandler[T]): ArtifactFrame[T] =
    handler(obj, ArtifactFrame) {
      newInstance[T](obj, mode = mode, initMode = initMode)
    }

  private def newInstance[T <: Txn[T]](obj: Artifact[T], mode: Boolean, initMode: FileDialog.Mode)
                                      (implicit tx: T, handler: UniverseHandler[T]): ArtifactFrame[T] = {
    implicit val undoMgr: UndoManager[T] = UndoManager()
    val view  = ArtifactView(obj, mode = mode, initMode = initMode)
    val name  = CellView.name(view.obj)
    val res   = new Impl(view, mode = mode, initMode = initMode)
    res.init().setTitle(name.map(n => s"$n : $humanName"))
  }

  private final class Impl[T <: Txn[T]](val view: ArtifactView[T], mode: Boolean, initMode: FileDialog.Mode)
                                       (implicit val handler: UniverseHandler[T])
    extends WorkspaceWindowImpl[T] with ArtifactFrame[T] {

    override def newWindow()(implicit tx: T): ArtifactFrame[T] =
      newInstance[T](view.obj, mode = mode, initMode = initMode)
  }
}