/*
 *  BooleanObjView.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite.impl.objview

import de.sciss.desktop
import de.sciss.lucre.expr.graph.Ex
import de.sciss.lucre.synth.Txn
import de.sciss.lucre.{BooleanObj, Expr, Source, Txn => LTxn}
import de.sciss.mellite.impl.ObjViewCmdLineParser
import de.sciss.mellite.impl.ObjViewCmdLineParser.collectBool
import de.sciss.mellite.impl.objview.ObjViewImpl.raphaelIcon
import de.sciss.mellite.{ObjListView, ObjView, Shapes}
import de.sciss.proc.{Code, Confluent, Universe}
import org.rogach.scallop

import javax.swing.Icon
import scala.swing.CheckBox

object BooleanObjView extends ObjListView.Factory with ProgramSupport {
  type E[T <: LTxn[T]] = BooleanObj[T]
  val icon          : Icon    = raphaelIcon(Shapes.BooleanNumber)
  val prefix        : String  = "Boolean"
  def humanName     : String  = prefix
  def category      : String  = ObjView.categPrimitives
  type Elem                   = Boolean

  def tpe     : Expr.Type[Elem, E] = BooleanObj
  val codeType: Code.TypeT[Unit, Ex[Elem]]  = Code.Program.Boolean

  override protected def scallopValueConverter: scallop.ValueConverter[Elem] =
    scallop.singleArgConverter(collectBool)

  def mkListView[T <: Txn[T]](obj: BooleanObj[T])(implicit tx: T): ObjListView[T] = {
    val ex          = obj
    val value       = ex.value
    val isEditable  = Expr.isVar(ex)
    val isProgram   = Expr.isProgram(ex)
    val isViewable = isProgram || tx.isInstanceOf[Confluent.Txn]
    new Impl[T](tx.newHandle(obj), value, isListCellEditable = isEditable, isProgram = isProgram,
      isViewable = isViewable).init(obj)
  }

  override def initMakeDialog[T <: Txn[T]](window: Option[desktop.Window])
                                          (implicit universe: Universe[T]): MakeResult[T] = {
    val ggValue     = new CheckBox()
    val codeValue0  = "false"
    showMakeDialog(ggValue = ggValue, codeValue0 = codeValue0, prefix = prefix,
      window = window)(ggValue.selected)
  }

  override def initMakeCmdLine[T <: Txn[T]](args: List[String])(implicit universe: Universe[T]): MakeResult[T] = {
    object p extends ObjViewCmdLineParser[Config[T]](this, args) {
      val const: Opt[Boolean] = opt     (descr = s"Make constant instead of variable")
      val value: Opt[Either[Elem, codeType.Repr]] = trailArg(descr = s"Initial $prefix value (0, 1, false, true, F, T)")
    }
    p.parseFut {
      prepareConfig[T](p.name(), p.value(), p.const())
    }
  }

  final class Impl[T <: Txn[T]](val objH: Source[T, BooleanObj[T]],
                                var value: Boolean,
                                override val isListCellEditable: Boolean, val isProgram: Boolean,
                                val isViewable: Boolean)
    extends BooleanObjView[T] with ListBase[T]
      with ObjListViewImpl.BooleanExprLike[T]
      with ObjListViewImpl.SimpleExpr[T, Elem, E] {

  }
}
trait BooleanObjView[T <: LTxn[T]] extends ObjView[T] {
  type Repr = BooleanObj[T]
}