/*
 *  AttrMapFrameImpl.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite.impl.document

import de.sciss.desktop
import de.sciss.lucre.Obj
import de.sciss.lucre.edit.{EditAttrMap, UndoManager}
import de.sciss.lucre.expr.CellView
import de.sciss.lucre.synth.Txn
import de.sciss.mellite.impl.WorkspaceWindowImpl
import de.sciss.mellite.impl.component.{CollectionViewImpl, NoMenuBarActions}
import de.sciss.mellite.{AttrMapFrame, AttrMapView, ObjView, UniverseHandler, ViewState}
import de.sciss.proc.Universe

import scala.swing.{Action, Component}

object AttrMapFrameImpl {
  def apply[T <: Txn[T]](obj: Obj[T])(implicit tx: T, handler: UniverseHandler[T]): AttrMapFrame[T] =
    handler(obj, AttrMapFrame)(newInstance(obj))

  private def newInstance[T <: Txn[T]](obj: Obj[T])(implicit tx: T, handler: UniverseHandler[T]): AttrMapFrame[T] = {
    import handler.universe
    implicit val undoMgr: UndoManager[T] = UndoManager()
    val contents  = AttrMapView[T](obj)
    val view      = new ViewImpl[T](contents)
    view.init()
    val name      = CellView.name(obj)
    val res       = new FrameImpl[T](view)
    res.init().setTitle(name.map(n => s"$n : Attributes"))
    res
  }

  private final class ViewImpl[T <: Txn[T]](val peer: AttrMapView[T])
                                           (implicit val undoManager: UndoManager[T],
                                            protected val universeHandler: UniverseHandler[T]
                                           )
    extends CollectionViewImpl[T] {

    impl =>

    override def obj(implicit tx: T): Obj[T] = peer.obj

    override def viewState: Set[ViewState] = peer.viewState

    //    def workspace: Workspace[T] = peer.workspace
    val universe: Universe[T] = peer.universe

    def dispose()(implicit tx: T): Unit = ()

    protected lazy val actionDelete: Action = Action(null) {
      val sel = peer.selection
      cursor.step { implicit tx =>
        val obj0 = peer.obj
        undoManager.capture("Delete Attributes") {
          sel.foreach { case (key, _) =>
            EditAttrMap.removeUndo(/*name = s"Delete Attribute '$key'",*/ obj0.attr, key = key)
          }
        }
      }
    }

    protected type InsertConfig = String

    protected def prepareInsertDialog(f: ObjView.Factory): Option[String] = peer.queryKey()

    protected def prepareInsertCmdLine(args: List[String]): Option[(String, List[String])] = args match {
      case key :: rest  => Some((key, rest))
      case _            => None
    }

    protected override def editInsert(f: ObjView.Factory, xs: List[Obj[T]], key: String)(implicit tx: T): Boolean = {
      xs.lastOption.exists { value =>
        // val editName = s"Create Attribute '$key'"
        EditAttrMap.putUndo(/*name = editName,*/ peer.obj.attr, key = key, value = value)
        true
      }
//      CompoundEdit(edits, "Create Attributes")
    }

    protected def initGUI2(): Unit = {
      peer.addListener {
        case AttrMapView.SelectionChanged(_, sel) =>
          selectionChanged(sel.map(_._2))
      }
    }

    protected def selectedObjects: List[ObjView[T]] = peer.selection.map(_._2)
  }

  private final class FrameImpl[T <: Txn[T]](val view: ViewImpl[T])(implicit val handler: UniverseHandler[T])
    extends AttrMapFrame[T] with WorkspaceWindowImpl[T] with NoMenuBarActions {

    override protected def style: desktop.Window.Style = desktop.Window.Auxiliary

    override def contents: AttrMapView[T] = view.peer

    def component: Component = contents.component

    override protected def viewStateKey: String = ViewState.Key_Attr

    override def supportsNewWindow: Boolean = false // since there is new menu bar...

    override def newWindow()(implicit tx: T): AttrMapFrame[T] =
      newInstance(view.obj)

    override protected def initGUI(): Unit = {
      super.initGUI()
      initNoMenuBarActions(component)
    }
  }
}