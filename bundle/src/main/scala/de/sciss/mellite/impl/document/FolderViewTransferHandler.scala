/*
 *  FolderViewTransferHandler.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite.impl.document

import de.sciss.audiofile.{AudioFile, AudioFileSpec}
import de.sciss.equal.Implicits._
import de.sciss.kollflitz.ISeq
import de.sciss.lucre.edit.{EditFolder, UndoManager}
import de.sciss.lucre.swing.TreeTableView
import de.sciss.lucre.{ArtifactLocation, Copy, Folder, Obj, Source, Txn}
import de.sciss.mellite.{DragAndDrop, FolderView, ObjListView, ObjView, ObjectActions, ActionArtifactLocation => Loc}
import de.sciss.proc.Universe

import java.awt.datatransfer.{DataFlavor, Transferable}
import java.io.File
import java.net.URI
import javax.swing.TransferHandler.TransferSupport
import javax.swing.{JComponent, TransferHandler}
import scala.collection.mutable
import scala.language.existentials
import scala.util.Try

/** Mixin that provides a transfer handler for the folder view. */
trait FolderViewTransferHandler[T <: Txn[T]] { fv =>
  protected implicit def undoManager: UndoManager[T]
  protected implicit val universe : Universe[T]

  protected def treeView: TreeTableView[T, Obj[T], Folder[T], ObjListView[T]]
  protected def selection: FolderView.Selection[T]

  protected def findLocation(f: URI, suggestions: ISeq[Loc.QueryResult[T]]): Option[Loc.QueryResult[T]]

  protected object FolderTransferHandler extends TransferHandler {
    // ---- export ----

    override def getSourceActions(c: JComponent): Int =
      TransferHandler.COPY | TransferHandler.MOVE | TransferHandler.LINK // dragging only works when MOVE is included. Why?

    override def createTransferable(c: JComponent): Transferable = {
      val sel     = selection
      val trans0  = DragAndDrop.Transferable(FolderView.SelectionFlavor) {
        new FolderView.SelectionDnDData[T](fv.universe, sel)
      }
      val trans1 = if (sel.size === 1) {
        val listView = sel.head.renderData
        val tmp0  = DragAndDrop.Transferable(ObjView.Flavor) {
          ObjView.Drag[T](fv.universe, listView, Set.empty)
        }
        val tmp1  = listView.createTransferable().toList
        val tmp   = trans0 :: tmp0 :: tmp1

        DragAndDrop.Transferable.seq(tmp: _*)
      } else trans0

      trans1
    }

    // ---- import ----

    override def canImport(support: TransferSupport): Boolean =
      treeView.dropLocation.exists { tdl =>
        val locOk = tdl.index >= 0 || (tdl.column === 0 && !tdl.path.lastOption.exists(_.isLeaf))
        val res = locOk && {
          if (support.isDataFlavorSupported(FolderView.SelectionFlavor)) {
            val data = support.getTransferable.getTransferData(FolderView.SelectionFlavor)
              .asInstanceOf[FolderView.SelectionDnDData[_]]
            if (data.universe != universe) {
              // no linking between sessions
              support.setDropAction(TransferHandler.COPY)
            }
            true
          } else if (support.isDataFlavorSupported(ObjView.Flavor)) {
            val data = support.getTransferable.getTransferData(ObjView.Flavor)
              .asInstanceOf[ObjView.Drag[_]]
            if (data.universe != universe) {
              // no linking between sessions
              support.setDropAction(TransferHandler.COPY)
            }
            true
          } else {
            support.isDataFlavorSupported(DataFlavor.javaFileListFlavor)
          }
        }
        // println(s"canImport? $res")
        res
      }

    override def importData(support: TransferSupport): Boolean =
      treeView.dropLocation.exists { _ =>
        // println("importData")
        val editOpt: Boolean = {
          val isFolder  = support.isDataFlavorSupported(FolderView.SelectionFlavor)
          val isList    = support.isDataFlavorSupported(ObjView.Flavor)

          // println(s"importData -- isFolder $isFolder, isList $isList")
          // println(support.getTransferable.getTransferDataFlavors.mkString("flavors: ", ", ", ""))

          val crossSessionFolder = isFolder &&
            (support.getTransferable.getTransferData(FolderView.SelectionFlavor)
              .asInstanceOf[FolderView.SelectionDnDData[_]].universe != universe)
          val crossSessionList = !crossSessionFolder && isList &&
            (support.getTransferable.getTransferData(ObjView.Flavor)
              .asInstanceOf[ObjView.Drag[_]].universe != universe)

          // println(s"importData -- crossSession ${crossSessionFolder | crossSessionList}")

          if (crossSessionFolder)
            copyFolderData(support)
          else if (crossSessionList) {
            copyListData(support)
          } else {
            import universe.cursor
            val pOpt = cursor.step { implicit tx => parentOption.map { case (f, fi) => (tx.newHandle(f), fi) }}
            // println(s"parentOption.isDefined? ${pOpt.isDefined}")
            pOpt.foldLeft(false) { case (acc, (parentH, idx)) =>
              if (isFolder) {
                insertFolderData(support, parentH, idx)
                true
              } else if (isList) {
                insertListData(support, parentH, idx)
                true
              } else if (support.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
                importFiles(support, parentH, idx)
                true
              } else acc
            }
          }
        }
        editOpt
      }

    // ---- folder: link ----

    private def insertFolderData(support: TransferSupport, parentH: Source[T, Folder[T]],
                                 index: Int): Boolean = {
      val data = support.getTransferable.getTransferData(FolderView.SelectionFlavor)
        .asInstanceOf[FolderView.SelectionDnDData[T]]

      insertFolderData1(data.selection, parentH, idx = index, dropAction = support.getDropAction)
    }

    // XXX TODO: not sure whether removal should be in exportDone or something
    private def insertFolderData1(sel0: FolderView.Selection[T], newParentH: Source[T, Folder[T]], idx: Int,
                                  dropAction: Int): Boolean = {
      // println(s"insert into $parent at index $idx")

      val sel = FolderView.cleanSelection(sel0)

      import de.sciss.equal.Implicits._

      val isMove = dropAction === TransferHandler.MOVE
      val isCopy = dropAction === TransferHandler.COPY

      universe.cursor.step { implicit tx =>
        val newParent = newParentH()

        def isNested(c: Obj[T]): Boolean = c match {
          case objT: Folder[T] =>
            objT === newParent || objT.iterator.toList.exists(isNested)
          case _ => false
        }

        // make sure we are not moving a folder within itself (it will magically disappear :)
        val sel1 = if (!isMove) sel else sel.filterNot(nv => isNested(nv.modelData()))

        // if we move children within the same folder, adjust the insertion index by
        // decrementing it for any child which is above the insertion index, because
        // we will first remove all children, then re-insert them.
        val idx0 = if (idx >= 0) idx else newParent /* .children */ .size
        val idx1 = if (!isMove) idx0
        else idx0 - sel1.count { nv =>
          val isInNewParent = nv.parent === newParent
          val child = nv.modelData()
          isInNewParent && newParent.indexOf(child) <= idx0
        }

        val prefix = if (isMove) "Move" else if (isCopy) "Copy" else "Link"
        val name = sel1 match {
          case single :: Nil  => single.renderData.humanName
          case _              => "Elements"
        }
        undoManager.capture(s"$prefix $name") {
          val editRemove = if (!isMove) false
          else sel1.foldLeft(false) { case (acc, nv) =>
            val parent: Folder[T] = nv.parent
            val childH = nv.modelData
            val idx = parent.indexOf(childH())
            if (idx < 0) {
              println("WARNING: Parent of drag object not found")
              acc
            } else {
              EditFolder.removeAtUndo[T](/*nv.renderData.humanName,*/ parent, idx /*, childH()*/)
              true
            }
          }

          val selZip = sel1.zipWithIndex
          val editInsert = selZip.nonEmpty
          if (isCopy) {
            val context = Copy[T, T]()
            selZip.foreach { case (nv, off) =>
              val in  = nv.modelData()
              val out = context(in)
              EditFolder.insertUndo[T](/*nv.renderData.humanName,*/ newParent, idx1 + off, child = out)
            }
            context.finish()
          } else {
            selZip.foreach { case (nv, off) =>
              EditFolder.insertUndo[T](/*nv.renderData.humanName,*/ newParent, idx1 + off, child = nv.modelData())
            }
          }

          val edits = editRemove || editInsert
          edits
        }
      }
    }

    // ---- folder: copy ----

    private def copyFolderData(support: TransferSupport): Boolean = {
      // cf. https://stackoverflow.com/questions/20982681
      val data = support.getTransferable.getTransferData(FolderView.SelectionFlavor)
        .asInstanceOf[FolderView.SelectionDnDData[In] forSome { type In <: Txn[In] }]
      copyFolderData1(data)
    }

    private def copyFolderData1[In <: Txn[In]](data: FolderView.SelectionDnDData[In]): Boolean =
      Txn.copy[In, T, Boolean] { (txIn: In, tx: T) => {
        parentOption(tx).exists { case (parent, idx) =>
          val sel0  = data.selection
          val sel   = FolderView.cleanSelection(sel0)
          copyFolderData2(sel, parent, idx)(txIn, tx)
        }
      }} (data.universe.cursor, fv.universe.cursor)

    private def copyFolderData2[In <: Txn[In]](sel: FolderView.Selection[In], newParent: Folder[T], idx: Int)
                                       (implicit txIn: In, tx: T): Boolean = {
      val idx1 = if (idx >= 0) idx else newParent.size
      val name = sel match {
        case single :: Nil  => single.renderData.humanName
        case _              => "Elements"
      }
      if (sel.isEmpty) false else undoManager.capture(s"Import $name From Other Workspace") {
        val context = Copy[In, T]()
        sel.zipWithIndex.foreach { case (nv, off) =>
          val in  = nv.modelData()
          val out = context(in)
          EditFolder.insertUndo[T](/*nv.renderData.humanName,*/ newParent, idx1 + off, child = out)
        }
        context.finish()
        true
      }
    }

    // ---- list: link ----

    private def insertListData(support: TransferSupport, parentH: Source[T, Folder[T]],
                               index: Int): Unit = {
      val data = support.getTransferable.getTransferData(ObjView.Flavor)
        .asInstanceOf[ObjView.Drag[T]]

      universe.cursor.step { implicit tx =>
        val parent = parentH()
        insertListData1(data, parent, idx = index, dropAction = support.getDropAction)
      }
    }

    // XXX TODO: not sure whether removal should be in exportDone or something
    private def insertListData1(data: ObjView.Drag[T], parent: Folder[T], idx: Int, dropAction: Int)
                               (implicit tx: T): Unit = {
      val idx1    = if (idx >= 0) idx else parent.size
      val nv      = data.view
      val in      = nv.obj
      EditFolder.insertUndo[T](/*nv.name,*/ parent, idx1, child = in)
    }

    // ---- list: copy ----

    private def copyListData(support: TransferSupport): Boolean = {
      // cf. https://stackoverflow.com/questions/20982681
      val data = support.getTransferable.getTransferData(ObjView.Flavor)
        .asInstanceOf[ObjView.Drag[In] forSome { type In <: Txn[In] }]
      copyListData1(data)
    }

    private def copyListData1[In <: Txn[In]](data: ObjView.Drag[In]): Boolean =
      Txn.copy[In, T, Boolean] { (txIn: In, tx: T) =>
        parentOption(tx).exists { case (parent, idx) =>
          implicit val txIn0 : In = txIn
          implicit val txOut0: T  = tx
          val idx1    = if (idx >= 0) idx else parent.size
          val context = Copy[In, T]()
          val nv      = data.view
          val in: Obj[In] = nv.obj
          val out     = context(in)
          EditFolder.insertUndo[T](/*nv.name,*/ parent, idx1, child = out)
          context.finish()
          true
        }
      } (data.universe.cursor, fv.universe.cursor)

    // ---- files ----

    private def importFiles(support: TransferSupport, parentH: Source[T, Folder[T]],
                            index: Int): Boolean = {
      import scala.collection.JavaConverters._
      val data: List[File] = support.getTransferable.getTransferData(DataFlavor.javaFileListFlavor)
        .asInstanceOf[java.util.List[File]].asScala.toList
      val tup: List[(URI, AudioFileSpec)] = data.flatMap { f =>
        Try(AudioFile.readSpec(f)).toOption.map(f.toURI -> _)
      }
      val (trip, _) =
        tup.foldLeft((Vector.empty[(URI, AudioFileSpec, Loc.QueryResult[T])], List.empty[Loc.QueryResult[T]])) {
          case (prev @ (entries, suggestions), (f, spec)) =>
          findLocation(f, suggestions) match {
            case Some(loc) =>
              val entry = (f, spec, loc)
              val newEntries  = entries :+ entry
              val newSug      = (loc :: suggestions).distinct
              (newEntries, newSug)
            case None => prev
          }
        }

      universe.cursor.step { implicit tx =>
        undoManager.capture("Insert Audio Files") {
          val parent = parentH()
          // keep track of which artifact locations have already been created
          var locMap  = Map.empty[Loc.QueryResult[T], (Option[Obj[T]], ArtifactLocation[T])]
          val locSet  = mutable.Set.empty[Obj[T]]
          var indexIt = index
          trip.foreach { case (f, spec, either) =>
            val locTupOpt = locMap.get(either).orElse(Loc.merge(either))
            locTupOpt.foreach { case locTup @ (xs, locM) =>
              locMap += either -> locTup
              xs.foreach { x =>
                if (locSet.add(x)) {
                  EditFolder.insertUndo[T](/*"Location",*/ parent, indexIt, x)
                  indexIt += 1
                }
              }
              val obj   = ObjectActions.mkAudioFile(locM, f, spec)
              EditFolder.insertUndo[T](/*"Audio File",*/ parent, indexIt, obj)
              indexIt += 1
            }
          }
          indexIt > index
        }
      }
    }

    private def parentOption(implicit tx: T): Option[(Folder[T], Int)] =
      treeView.dropLocation.flatMap { tdl =>
        val parentOpt = tdl.path.lastOption.fold(Option(treeView.root())) { nodeView =>
          nodeView.modelData() match {
            case objT: Folder[T]  => Some(objT)
            case _                => None
          }
        }
        parentOpt.map(_ -> tdl.index)
      }
  }
}