/*
 *  IntVectorObjView.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite.impl.objview

import de.sciss.desktop
import de.sciss.kollflitz.Vec
import de.sciss.lucre.expr.graph.Ex
import de.sciss.lucre.synth.Txn
import de.sciss.lucre.{Expr, IntVector, Source, Txn => LTxn}
import de.sciss.mellite.impl.ObjViewCmdLineParser
import de.sciss.mellite.impl.objview.ObjViewImpl.raphaelIcon
import de.sciss.mellite.{MessageException, ObjListView, ObjView, Shapes}
import de.sciss.proc.{Code, Confluent, Universe}
import org.rogach.scallop

import javax.swing.Icon
import scala.swing.TextField
import scala.util.Try
import scala.util.control.NonFatal

object IntVectorObjView extends ObjListView.Factory with ProgramSupport {
  type E[T <: LTxn[T]] = IntVector[T]
  val icon          : Icon    = raphaelIcon(Shapes.IntegerNumberVector)
  val prefix        : String  = "IntVector"
  def humanName     : String  = "Int Vector"
  def category      : String  = ObjView.categPrimitives
  type A                      = Int
  type Elem                   = Vec[A]

  def tpe     : Expr.Type[Elem, E] = IntVector
  val codeType: Code.TypeT[Unit, Ex[Elem]] = Code.Program.IntVec

  override protected def scallopValueConverter: scallop.ValueConverter[Elem] =
    scallop.singleArgConverter(s => ObjViewCmdLineParser.collectVec[A](s).get)

  def mkListView[T <: Txn[T]](obj: E[T])(implicit tx: T): IntVectorObjView[T] with ObjListView[T] = {
    val ex          = obj
    val value       = ex.value
    val isEditable  = Expr.isVar(ex)
    val isProgram   = Expr.isProgram(ex)
    val isViewable = isProgram || tx.isInstanceOf[Confluent.Txn]
    new Impl[T](tx.newHandle(obj), value, isListCellEditable = isEditable, isProgram = isProgram,
      isViewable = isViewable).init(obj)
  }

  private def parseString(s: String): Vec[Int] =
    try {
      s.split(",").iterator.map(x => x.trim().toInt).toIndexedSeq
    } catch {
      case NonFatal(_) =>
        throw MessageException(s"Cannot parse '$s' as $humanName")
    }

  override def initMakeDialog[T <: Txn[T]](window: Option[desktop.Window])
                                          (implicit universe: Universe[T]): MakeResult[T] = {
    val ggValue     = new TextField("0,0")
    val codeValue0  = "Seq.empty[Int]"
    showMakeDialog(ggValue = ggValue, codeValue0 = codeValue0, prefix = prefix,
      window = window)(parseString(ggValue.text))
  }

  override def initMakeCmdLine[T <: Txn[T]](args: List[String])(implicit universe: Universe[T]): MakeResult[T] = {
    object p extends ObjViewCmdLineParser[Config[T]](this, args) {
      val const: Opt[Boolean]   = opt   (descr = s"Make constant instead of variable")
      val value: Opt[Either[Elem, codeType.Repr]] = trailArg(descr = s"Comma-separated list of $prefix values (, for empty list)")
    }
    p.parseFut {
      prepareConfig[T](p.name(), p.value(), p.const())
    }
  }

  final class Impl[T <: Txn[T]](val objH: Source[T, E[T]], value0: Elem,
                                override val isListCellEditable: Boolean, val isProgram: Boolean,
                                val isViewable: Boolean)
    extends IntVectorObjView[T] with ListBase[T]
      with ObjListViewImpl.VectorExpr[T, A, E] {

    protected var exprValue: Elem = value0

    def value: String =
      exprValue.mkString(",")

    def convertEditValue(v: Any): Option[Vec[Int]] = v match {
      case num: Vec[Any] =>
        num.foldLeft(Option(Vec.empty[Int])) {
          case (Some(prev), d: Int) => Some(prev :+ d)
          case _                    => None
        }

      case s: String  => Try { parseString(s) }.toOption
      case _          => None
    }
  }
}
trait IntVectorObjView[T <: LTxn[T]] extends ObjView[T] {
  type Repr = IntVector[T]
}