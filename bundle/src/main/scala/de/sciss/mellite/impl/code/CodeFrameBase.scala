/*
 *  CodeFrameBase.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite.impl.code

import de.sciss.desktop.{Menu, OptionPane}
import de.sciss.kollflitz.ISeq
import de.sciss.lucre.Txn
import de.sciss.lucre.swing.LucreSwing.deferTx
import de.sciss.mellite.impl.WindowImpl
import de.sciss.mellite.{CodeView, Veto}
import de.sciss.proc.Code.Example
import de.sciss.processor.Processor.Aborted

import scala.concurrent.{Future, Promise}
import scala.swing.Button
import scala.swing.event.Key

/** Building block for code frames that vetoes closing when
  * code is not saved or compilation not finished.
  *
  * Also provides menu generation for examples.
  */
trait CodeFrameBase[T <: Txn[T]] extends Veto[T] {
  this: WindowImpl[T] =>

  protected def codeView: CodeView[T, _]

  override def prepareDisposal()(implicit tx: T): Option[Veto[T]] =
    if (!codeView.isCompiling && !codeView.dirty) None else Some(this)

  private def _vetoMessage = "The code has been edited."

  private class ExampleAction(ex: Example) extends swing.Action(ex.name) {
    if (ex.mnemonic != 0) mnemonic = ex.mnemonic

    def apply(): Unit =
      codeView.currentText = ex.code
  }

  def vetoMessage(implicit tx: T): String =
    if (codeView.isCompiling) "Ongoing compilation." else _vetoMessage

  def tryResolveVeto()(implicit tx: T): Future[Unit] =
    if (codeView.isCompiling) Future.failed(Aborted())
    else {
      val p = Promise[Unit]()
      deferTx {
        val message = s"${_vetoMessage}\nSave changes before closing?"

        val Yes     = 0
        val No      = 1
        val Cancel  = 2
        val Closed  = -1
        val Swap    = 3

        val tpeEntries = Seq.tabulate(4) { value =>
          val (txt, key) = (value: @unchecked) match {
            // keep them short, because it gets too wordy
            case Yes      => ("Yes", Key.Y) // ("Apply and Save"       , Key.S)
            case No       => ("No" , Key.N) // ("Close without Saving" , Key.N)
            case Cancel   => ("Cancel"               , Key.C)
            case Swap     => ("Keep as Swap"         , Key.K)
          }
          val b = new Button(txt)
          b.mnemonic = key
          b
        }

        val opt = OptionPane.buttons(message = message, messageType = OptionPane.Message.Warning,
          options = tpeEntries, initial = Yes)
        val wTit0 = title
        val wTit  = if (wTit0.startsWith("*") || wTit0.startsWith("~")) wTit0.substring(1) else wTit0
        opt.title = s"Close - $wTit"
        val res = opt.show(Some(window))
        (res: @unchecked) match {
          case Yes /*OptionPane.Result.Yes*/ =>
            val fut = codeView.save()
            p.completeWith(fut)
          case No /*OptionPane.Result.No*/ =>
            p.success(())
          case Cancel /*OptionPane.Result.Cancel*/ | Closed /*OptionPane.Result.Closed*/ =>
            p.failure(Aborted())
          case Swap =>
            val fut = codeView.saveSwap()
            p.completeWith(fut)
        }
      }
      p.future
    }

  protected final def mkExamplesMenu(examples: ISeq[Example]): Unit =
    if (examples.nonEmpty) {
      val gEx = Menu.Group("examples", "Examples")
      gEx.action.mnemonic = 'x'
      val _code = codeView
      _code.addListener {
        case CodeView.VisibilityChange(b) =>
          gEx.action.enabled = b
      }
      gEx.action.enabled = false // always `false` initially; _code.visible
      examples.iterator.zipWithIndex.foreach { case (ex, i) =>
        val aEx = new ExampleAction(ex)
        gEx.add(Menu.Item(s"example-$i", aEx))
      }
      val mf = window.handler.menuFactory
      mf.add(Some(window), gEx)
    }
}
