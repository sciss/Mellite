/*
 *  TimelineActions.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite.impl.timeline

import de.sciss.desktop.KeyStrokes.menu2
import de.sciss.desktop.{KeyStrokes, OptionPane, Window}
import de.sciss.fingertree.RangedSeq
import de.sciss.lucre.edit.EditTimeline
import de.sciss.lucre.synth.Txn
import de.sciss.lucre.{IntObj, Obj, SpanLikeObj, StringObj}
import de.sciss.mellite.edit.Edits
import de.sciss.mellite.impl.TimelineViewBaseImpl
import de.sciss.mellite.impl.proc.ProcObjView
import de.sciss.mellite.{ActionBounce, ObjTimelineView, ProcActions, TimelineView}
import de.sciss.proc.{ObjKeys, Tag, TimeRef, Timeline}
import de.sciss.span.Span
import de.sciss.{proc, topology}
import de.sciss.topology.Topology

import scala.swing.Action
import scala.swing.event.Key
import scala.util.Success

/** Implements the actions defined for the timeline-view. */
trait TimelineActions[T <: Txn[T]] {
  view: TimelineView[T] with TimelineViewBaseImpl[T, Int, ObjTimelineView[T]] =>

  object actionStopAllSound extends Action("StopAllSound") {
    def apply(): Unit =
      cursor.step { implicit tx =>
        transportView.transport.stop()  // XXX TODO - what else could we do?
        // auralView.stopAll
      }
  }

  object actionBounce extends ActionBounce(this, objH) {

    override protected def prepare(set0: ActionBounce.QuerySettings[T],
                                   recalled: Boolean): ActionBounce.QuerySettings[T] =
      if (timelineModel.selection.isEmpty) set0 else set0.copy(span = timelineModel.selection)

    override protected def spanPresets(): SpanPresets = {
      val all = cursor.step { implicit tx =>
        ActionBounce.presetAllTimeline(obj)
      }
      val sel = timelineModel.selection.nonEmptyOption.map(value => ActionBounce.SpanPreset("Selection", value)).toList
      all ::: sel
    }
  }

  object actionSelectAll extends Action("Select All") {
    def apply(): Unit = {
      canvas.iterator.foreach { view =>
        if (!selectionModel.contains(view)) selectionModel += view
      }
    }
  }

  object actionSelectFollowing extends Action("Select Following Objects") {
    accelerator = Some(menu2 + Key.F)
    def apply(): Unit = {
      selectionModel.clear()
      val pos = timelineModel.position
      canvas.intersect(Span.from(pos)).foreach { view =>
        // XXX TODO --- this is an additional filter we need because `intersect` doesn't allow >= condition for start
        val ok = view.spanValue match {
          case hs: Span.HasStart => hs.start >= pos
          case _ => false
        }
        if (ok) selectionModel += view
      }
    }
  }

  object actionDelete extends Action("Delete") {
    def apply(): Unit =
      withSelection { implicit tx => views =>
        undoManager.capture(title) {
          timelineMod.foreach { tlm =>
            views.foreach { pv0 =>
              val span  = pv0.span
              val child = pv0.obj
              EditTimeline.unlinkAndRemoveUndo(tlm, span, elem = child)
            }
          }
          None
        }
      }
  }

  object actionSplitObjects extends Action("Split Selected Objects") {
    import KeyStrokes.menu2
    accelerator = Some(menu2 + Key.Y)
    enabled     = false

    def apply(): Unit = {
      val pos     = timelineModel.position
      val pos1    = pos - TimelineView.MinDur
      val pos2    = pos + TimelineView.MinDur
      withFilteredSelection(pv => pv.spanValue.contains(pos1) && pv.spanValue.contains(pos2)) { implicit tx =>
        it => {
          splitObjects(pos)(it)
          None
        }
      }
    }
  }

  object actionCleanUpObjects extends Action("Vertically Arrange Overlapping Objects") {
    enabled = false

    def apply(): Unit = {
      type V = ObjTimelineView[T]
      case class E(sourceVertex: V, targetVertex: V) extends topology.Edge[V]
      case class Placed(view: ObjTimelineView[T], y: Int) {
        def nextY         : Int     = view.trackHeight + y + 1
        def deltaY        : Int     = y - view.trackIndex
        def isSignificant : Boolean = deltaY != 0
      }

      if (selectionModel.nonEmpty) {
        val sel     = selectionModel.iterator
        val top0    = sel.foldLeft(Topology.empty[V, E])(_ addVertex _)
        val viewSet = top0.vertices.toSet
        cursor.step { implicit tx =>
          undoManager.capture(title) {
            val top = top0.vertices.foldLeft(top0) {
              case (topIn, pv: ProcObjView.Timeline[T]) =>
                val targetIt = pv.targets.iterator.map(_.attr.parent).filter(viewSet.contains)
                targetIt.foldLeft(topIn) { case (topIn1, target) =>
                  topIn1.addEdge(E(pv, target)) match {
                    case Success((topNew, _)) => topNew
                    case _                    => topIn1
                  }
                }

              case (topIn, _) => topIn
            }

            val range0  = RangedSeq.empty[Placed, Long](pl => ObjTimelineView.spanToPoint(pl.view.spanValue), Ordering.Long)
            val pl0     = List.empty[Placed]
            val (_, plRes) = top.vertices.indices.foldLeft((range0, pl0)) { case ((rangeIn, plIn), viewIdx) =>
              val view      = top.vertices(viewIdx)
              val tup       = ObjTimelineView.spanToPoint(view.spanValue)
              val it0       = rangeIn.filterOverlaps(tup).map(_.nextY)
              val it1       = if (viewIdx < top.unconnected || plIn.isEmpty) it0 else it0 ++ Iterator.single(plIn.head.nextY)
              val nextY     = if (it1.isEmpty) 0 else it1.max
              val pl        = Placed(view, nextY)
              val rangeOut  = rangeIn + pl
              val plOut     = pl :: plIn
              (rangeOut, plOut)
            }

            val tl = obj
            plRes.foreach { pl =>
              if (pl.isSignificant) {
                val move = ProcActions.Move(deltaTime = 0L, deltaTrack = pl.deltaY, copy = false)
                Edits.timelineMoveOrCopy(pl.view.span, pl.view.obj, tl, move, minStart = Long.MinValue)
                ()
              }
            }
          }
        }
      }
    }
  }

//  object actionClearSpan extends Action("Clear Selected Span") {
//    import KeyStrokes._
//    accelerator = Some(menu1 + Key.BackSlash)
//    enabled     = false
//
//    def apply(): Unit =
//      timelineModel.selection.nonEmptyOption.foreach { selSpan =>
//        val editOpt = cursor.step { implicit tx =>
//          timelineMod.flatMap { groupMod =>
//            editClearSpan(groupMod, selSpan)
//          }
//        }
//        editOpt.foreach(undoManager.add)
//      }
//  }

//  object actionRemoveSpan extends Action("Remove Selected Span") {
//    import KeyStrokes._
//    accelerator = Some(menu1 + shift + Key.BackSlash)
//    enabled = false
//
//    def apply(): Unit = {
//      timelineModel.selection.nonEmptyOption.foreach { selSpan =>
//        val minStart = TimelineNavigation.minStart(timelineModel)
//        val editOpt = cursor.step { implicit tx =>
//          timelineMod.flatMap { groupMod =>
//            // ---- remove ----
//            // - first call 'clear'
//            // - then move everything right of the selection span's stop to the left
//            //   by the selection span's length
//            val editClear = editClearSpan(groupMod, selSpan)
//            val affected  = groupMod.intersect(Span.From(selSpan.stop))
//            val amount    = ProcActions.Move(deltaTime = -selSpan.length, deltaTrack = 0, copy = false)
//            val editsMove = affected.flatMap {
//              case (_ /* elemSpan */, elems) =>
//                elems.flatMap { timed =>
//                  Edits.timelineMoveOrCopy(timed.span, timed.value, groupMod, amount, minStart = minStart)
//                }
//            } .toList
//
//            CompoundEdit(editClear.toList ++ editsMove, title)
//          }
//        }
//        editOpt.foreach(undoManager.add)
//        timelineModel.modifiableOption.foreach { tlm =>
//          tlm.selection = Span.Void
//          tlm.position  = selSpan.start
//        }
//      }
//    }
//  }

  object actionAlignObjectsToCursor extends Action("Align Objects Start To Cursor") {
    enabled = false

    def apply(): Unit = {
      val pos = timelineModel.position
      withSelection { implicit tx => views =>
        val tl = obj
        undoManager.capture(title) {
          views.toIterator.foreach { _view =>
            val span = _view.span
            span.value match {
              case hs: Span.HasStart if hs.start != pos =>
                val delta   = pos - hs.start
                val amount  = ProcActions.Move(deltaTime = delta, deltaTrack = 0, copy = false)
                Edits.timelineMoveOrCopy(span, _view.obj, tl, amount = amount, minStart = 0L)
              case _ => None
            }
          }
        }
        None
      }
    }
  }

  object actionDropMarker extends Action("Drop Marker") {
    import KeyStrokes._
    accelerator = Some(plain + Key.M)

    final val name        = "Mark"
    final val trackHeight = 2       // marker height

    private def markerSpan(): Span =
      timelineModel.selection match {
        case s: Span    => s
        case Span.Void  =>
          val start = timelineModel.position
          val stop  = start + TimeRef.SampleRate.toLong
          Span(start, stop)
      }

    private def dropTrack(span: Span): Int = {
      val spc = 1     // extra vertical spacing
      val pos = canvas.intersect(span).toList.sortBy(_.trackIndex).foldLeft(0) { (pos0, _view) =>
        val y1 = _view.trackIndex - spc
        val y2 = _view.trackIndex + _view.trackHeight + spc
        if (y1 >= (pos0 + trackHeight) || y2 <= pos0) pos0 else y2
      }
      pos
    }

    def locate(): (Span, Int) = {
      val span    = markerSpan()
      val trkIdx  = dropTrack(span)
      (span, trkIdx)
    }

    def apply(): Unit =
      perform(locate(), name = name)

    def perform(location: (Span, Int), name: String): Unit =
      cursor.step { implicit tx =>
        timelineMod.map { tlMod =>
          val (span, trkIdx) = location
          val spanObj = SpanLikeObj.newVar[T](span)
          val elem: Obj[T] = Tag[T]()
          val attr    = elem.attr
          attr.put(ObjTimelineView.attrTrackIndex , IntObj   .newVar[T](trkIdx      ))
          attr.put(ObjTimelineView.attrTrackHeight, IntObj   .newVar[T](trackHeight ))
          attr.put(ObjKeys.attrName               , StringObj.newVar[T](name        ))
          EditTimeline.add(/*name = "Drop Marker",*/ tlMod, span = spanObj, elem = elem)
        }
      }
  }

  object actionDropNamedMarker extends Action("Drop Named Marker") {
    import KeyStrokes._

    accelerator = Some(shift + Key.M)

    def apply(): Unit = {
      val loc = actionDropMarker.locate()
      val opt = OptionPane.textInput("Name:", initial = actionDropMarker.name)
      opt.title = title
      Window.showDialog(component, opt).foreach { name =>
        actionDropMarker.perform(loc, name)
      }
    }
  }

  // -----------

  protected def timelineMod(implicit tx: T): Option[Timeline.Modifiable[T]] =
    obj.modifiableOption

  // ---- clear ----
  // - find the objects that overlap with the selection span
  // - if the object is contained in the span, remove it
  // - if the object overlaps the span, split it once or twice,
  //   then remove the fragments that are contained in the span
  protected def editClearSpan(groupMod: proc.Timeline.Modifiable[T], selSpan: Span)
                             (implicit tx: T): Boolean = {
    var edited = false
    undoManager.capture("Clear Span") {
      groupMod.intersect(selSpan).foreach {
        case (elemSpan, elems) =>
          elems.foreach { timed =>
            if (selSpan contains elemSpan) {
              EditTimeline.unlinkAndRemoveUndo(groupMod, timed.span, timed.value)
              edited = true
            } else {
              timed.span match {
                case SpanLikeObj.Var(oldSpan) =>
                  val split1 = EditTimeline.splitUndo(groupMod, oldSpan , timed.value , selSpan.start)
                  val span2 = split1.rightSpan
                  val obj2  = split1.rightObj
                  if (!(selSpan contains span2.value)) {
                    EditTimeline.splitUndo(groupMod, span2, obj2, selSpan.stop  )
                  }
                  EditTimeline.unlinkAndRemoveUndo(groupMod, span2, obj2)
                  edited = true

                case _ => ()
              }
            }
          }
      }
    }
    edited
  }

  protected def splitObjects(time: Long)(views: TraversableOnce[ObjTimelineView[T]])
                  (implicit tx: T): Boolean =
    timelineMod.exists { groupMod =>
      var edited = false
      undoManager.capture("Split Objects") {
        views.toIterator.foreach { pv =>
          pv.span match {
            case SpanLikeObj.Var(oldSpan) =>
              EditTimeline.splitUndo(groupMod, oldSpan, pv.obj, time)
              edited = true
            case _ => ()
          }
        }
      }
      edited
    }
}