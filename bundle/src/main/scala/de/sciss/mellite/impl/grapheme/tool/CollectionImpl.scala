/*
 *  CollectionImpl.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite.impl.grapheme.tool

import de.sciss.lucre.edit.UndoManager
import de.sciss.lucre.synth.Txn
import de.sciss.lucre.{LongObj, Obj}
import de.sciss.mellite.impl.tool.BasicCollectionTool
import de.sciss.mellite.{GraphemeCanvas, GraphemeTool, ObjGraphemeView}
import de.sciss.proc.Grapheme

/** A more complete implementation for grapheme tools that process selected views.
  * It implements `commit` by aggregating individual view based
  * commits performed in the abstract method `commitObj`.
  */
trait CollectionImpl[T <: Txn[T], A] extends BasicCollectionTool[T, A, Double, ObjGraphemeView[T]]
  with GraphemeTool[T, A] {

  override protected def canvas: GraphemeCanvas[T]

  override def commit(drag: A)(implicit tx: T, undoManager: UndoManager[T]): Boolean = {
    lazy val parent = canvas.grapheme
    var edited = false
    undoManager.capture(name) {
      canvas.selectionModel.iterator.foreach { childView =>
        val time  = childView.time
        val child = childView.obj
        edited |= commitObj(drag)(time = time, child = child, parent = parent)
      }
    }
    edited
  }

  protected def commitObj(drag: A)(time: LongObj[T], child: Obj[T], parent: Grapheme[T])
                         (implicit tx: T, undoManager: UndoManager[T]): Boolean
}
