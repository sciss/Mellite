/*
 *  IntObjView.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite.impl.objview

import de.sciss.lucre.{Expr, IntObj, Source, Txn => LTxn}
import de.sciss.lucre.expr.graph.Ex
import de.sciss.lucre.synth.Txn
import de.sciss.mellite.{ObjListView, ObjView, Shapes}
import de.sciss.proc.{Code, Confluent, Universe}
import de.sciss.swingplus.Spinner
import de.sciss.desktop
import org.rogach.scallop

import javax.swing.{Icon, SpinnerNumberModel}
import scala.util.Try

object IntObjView extends ObjListView.Factory with ProgramSupport {
  type E[~ <: LTxn[~]] = IntObj[~]
  val icon          : Icon    = ObjViewImpl.raphaelIcon(Shapes.IntegerNumber)
  val prefix        : String  = "Int"
  def humanName     : String  = prefix
  def category      : String  = ObjView.categPrimitives
  type Elem                   = Int

  def tpe     : Expr.Type[Elem, E] = IntObj
  val codeType: Code.TypeT[Unit, Ex[Elem]]  = Code.Program.Int

  override protected def scallopValueConverter: scallop.ValueConverter[Elem] = scallop.intConverter

  def mkListView[T <: Txn[T]](obj: IntObj[T])(implicit tx: T): IntObjView[T] with ObjListView[T] = {
    val ex          = obj
    val value       = ex.value
    val isEditable  = Expr.isVar(ex)
    val isProgram   = Expr.isProgram(ex)
    val isViewable = isProgram || tx.isInstanceOf[Confluent.Txn]
    new ListImpl(tx.newHandle(obj), value, isListCellEditable = isEditable, isProgram = isProgram,
      isViewable = isViewable).init(obj)
  }

  override def initMakeDialog[T <: Txn[T]](window: Option[desktop.Window])
                                          (implicit universe: Universe[T]): MakeResult[T] = {
    val model       = new SpinnerNumberModel(0, Int.MinValue, Int.MaxValue, 1)
    val ggValue     = new Spinner(model)
    val codeValue0  = "0"
    showMakeDialog(ggValue = ggValue, codeValue0 = codeValue0, prefix = prefix,
      window = window)(model.getNumber.intValue())
  }

  private final class ListImpl[T <: Txn[T]](val objH: Source[T, E[T]],
                                            var value: Elem, val isProgram: Boolean,
                                            override val isListCellEditable: Boolean, val isViewable: Boolean)
    extends IntObjView[T]
    with ListBase[T]
    with ObjListViewImpl.SimpleExpr[T, Elem, E] with StringRenderer {

    def convertEditValue(v: Any): Option[Int] = v match {
      case num: Elem    => Some(num)
      case s  : String  => Try(s.toInt).toOption
      case _            => None
    }
  }
}
trait IntObjView[T <: LTxn[T]] extends ObjView[T] {
  type Repr = IntObj[T]
}