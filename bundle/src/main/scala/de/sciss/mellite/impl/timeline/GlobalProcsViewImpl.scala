/*
 *  GlobalProcsViewImpl.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite.impl.timeline

import de.sciss.desktop.{Menu, OptionPane}
import de.sciss.file.File
import de.sciss.icons.raphael
import de.sciss.lucre.edit.{EditAttrMap, EditTimeline, UndoManager}
import de.sciss.lucre.swing.LucreSwing.deferTx
import de.sciss.lucre.swing.impl.ComponentHolder
import de.sciss.lucre.synth.Txn
import de.sciss.lucre.{DoubleObj, IntObj, Obj, Source, SpanLikeObj, StringObj, TxnLike, Txn => LTxn}
import de.sciss.mellite.edit.Edits
import de.sciss.mellite.impl.objview.IntObjView
import de.sciss.mellite.impl.proc.ProcObjView
import de.sciss.mellite.impl.state.TableViewState
import de.sciss.mellite.{AttrMapFrame, DragAndDrop, GUI, GlobalProcsView, ObjTimelineView, ObjView, ProcActions, SelectionModel, TimelineTool, UniverseHandler, ViewState}
import de.sciss.proc.{ObjKeys, Proc, Timeline, Universe}
import de.sciss.span.Span
import de.sciss.swingplus.{ComboBox, GroupPanel}
import de.sciss.{desktop, equal}

import java.awt.datatransfer.Transferable
import java.awt.event.{MouseEvent => AWTMouseEvent}
import java.awt.event.MouseAdapter
import java.util.Comparator
import javax.swing.TransferHandler.TransferSupport
import javax.swing.table.{AbstractTableModel, TableColumnModel, TableModel, TableRowSorter}
import javax.swing.{DropMode, JComponent, SwingUtilities, TransferHandler}
import scala.annotation.switch
import scala.collection.immutable.{IndexedSeq => Vec}
import scala.collection.{Set => CSet}
import scala.concurrent.stm.TxnExecutor
import scala.swing.event.{MouseButtonEvent, MouseEvent, SelectionChanged, TableRowsSelected}
import scala.swing.{Action, BorderPanel, BoxPanel, Button, Component, FlowPanel, Label, Orientation, ScrollPane, Swing, Table, TextField}

object GlobalProcsViewImpl extends GlobalProcsView.Companion {
  def install(): Unit =
    GlobalProcsView.peer = this

  def apply[T <: Txn[T]](group: Timeline[T], selectionModel: SelectionModel[T, ObjTimelineView[T]])
                        (implicit tx: T, handler: UniverseHandler[T],
                         undo: UndoManager[T]): GlobalProcsView[T] = {

    // import ProcGroup.Modifiable.serializer
    val groupHOpt = group.modifiableOption.map(gm => tx.newHandle(gm))
    val view      = new Impl[T](/* tx.newHandle(group), */ groupHOpt, selectionModel)
    view.init(group)
  }

  private final val Key_ColWidths  = "global-col-widths"
  private final val Key_RowSort    = "global-row-sort"
  private final val Key_ColOrder   = "global-col-order"

  private final class Impl[T <: Txn[T]](// groupH: Source[T, Timeline[T]],
                                        groupHOpt: Option[Source[T, Timeline.Modifiable[T]]],
                                        tlSelModel: SelectionModel[T, ObjTimelineView[T]])
                                       (implicit handler: UniverseHandler[T],
                                        val undoManager: UndoManager[T])
    extends GlobalProcsView[T] with ComponentHolder[Component] {

    type C = Component

    private val stateTable = new TableViewState[T](
      keyColWidths = Key_ColWidths,
      keyRowSort   = Key_RowSort,
      keyColOrder  = Key_ColOrder
    )

    override implicit val universe: Universe[T] = handler.universe

    override def viewState: Set[ViewState] = stateTable.entries()

    //    private[this] var procSeq = Vec.empty[ProcObjView.Timeline[T]]
    private[this] var procSeq = Vec.empty[ProcObjView.Timeline[T]]

    private def atomic[A](block: T => A): A = cursor.step(block)

    private[this] var table: Table = _

    override def tableComponent: Table = table

    override val selectionModel: SelectionModel[T, ObjView[T]] = SelectionModel.apply

    private def tableSelection[R >: ProcObjView.Timeline[T]](): CSet[R] = {
      val rows = table.selection.rows
      rows.map { viewRow =>
        val modelRow  = table.viewToModelRow(viewRow)
        val pv        = procSeq(modelRow)
        pv: R
      }
    }

    private[this] val tlSelListener: SelectionModel.Listener[T, ObjTimelineView[T]] = {
      case SelectionModel.Update(_, _) =>
        val items: Set[ProcObjView.Timeline[T]] = TxnExecutor.defaultAtomic { implicit itx =>
          implicit val tx: TxnLike = LTxn.wrap(itx)
          tlSelModel.iterator.flatMap {
            case pv: ProcObjView.Timeline[T] =>
              pv.targets.flatMap { link =>
                val tgt = link.attr.parent
                if (tgt.isGlobal) Some(tgt) else None
              }

            case _ => Nil
          } .toSet
        }

        val indices = items.map { it =>
          val modelRow = procSeq.indexOf(it)
          table.modelToViewRow(modelRow)
        }
        val rows      = table.selection.rows
        val toAdd     = indices.diff(rows)
        val toRemove  = rows   .diff(indices)

        if (toRemove.nonEmpty) rows --= toRemove
        if (toAdd   .nonEmpty) rows ++= toAdd
    }

    // columns: name, gain, muted, bus
    private val tm = new AbstractTableModel {
      def getRowCount   : Int = procSeq.size
      def getColumnCount: Int = 4

      def getValueAt(row: Int, col: Int): AnyRef = {
        val pv  = procSeq(row)
        val res = (col: @switch) match {
          case 0 => pv.name
          case 1 => pv.gain
          case 2 => pv.muted
          case 3 => pv.busOption.getOrElse(0)
        }
        res.asInstanceOf[AnyRef]
      }

      override def isCellEditable(row: Int, col: Int): Boolean = true

      override def setValueAt(value: Any, row: Int, col: Int): Unit = {
        // rows are model-rows
        println(s"setValueAt($value (${value.getClass}), $row, $col)")
        val pv = procSeq(row)
        (col, value) match {
          case (0, name: String) =>
            atomic { implicit tx =>
              val editName = "Rename"
              Edits.adjustAttr[T, String, StringObj](pv.obj, key = ObjKeys.attrName, arg = name,
                editName = editName, default = "")((_, now) => now)
//              val nameOpt = if (name.isEmpty) None else Some(StringObj.newVar[T](name))
//              Edits.setName(pv.obj, if (name.isEmpty) None else Some(name))
              // ProcActions.rename(pv.obj, if (name.isEmpty) None else Some(name))
            }
          case (1, amount: Double) =>
            atomic { implicit tx =>
              val editName = "Adjust Gain"
              Edits.adjustAttr[T, Double, DoubleObj](pv.obj, key = ObjKeys.attrGain, arg = amount,
                editName = editName, default = 1.0)((_, now) => now)
              // ProcActions.setGain(pv.obj, factor)
            }

          case (2, engaged: Boolean) =>
            atomic { implicit tx =>
              Edits.mute(pv.obj, TimelineTool.Mute(engaged))
              // ProcActions.toggleMute(pv.obj)
            }

          case (3, bus: Int) =>   // XXX TODO: should use spinner for editing
            atomic { implicit tx =>
              val editName = "Adjust Bus"
              Edits.adjustAttr[T, Int, IntObj](pv.obj, key = ObjKeys.attrBus, arg = bus, editName = editName,
                default = 0)((_, now) => now)
              // ProcActions.setBus(pv.obj :: Nil, IntObj.newConst[T](bus))
            }

          case _ =>
        }
      }

      override def getColumnName(col: Int): String = (col: @switch) match {
        case 0 => "Name"
        case 1 => "Gain"
        case 2 => "M" // short because column only uses checkbox
        case 3 => "Bus"
        // case other => super.getColumnName(col)
      }

      override def getColumnClass(col: Int): Class[_] = (col: @switch) match {
        case 0 => classOf[String]
        case 1 => classOf[java.lang.Double]
        case 2 => classOf[Boolean]
        case 3 => classOf[java.lang.Integer]
      }
    }

    private def addItemWithDialog(): Unit =
      groupHOpt.foreach { groupH =>
        val lbName    = new Label("Name:")
        val ggName    = new TextField("Bus", 12)
        val lbPreset  = new Label("Preset:")
        val ggPreset  = new ComboBox(GlobalProcPreset.all) {
          listenTo(selection)
        }
        val flow      = new BoxPanel(Orientation.Vertical)
        val op        = OptionPane(flow, OptionPane.Options.OkCancel, focus = Some(ggName))

        var presetCtl: GlobalProcPreset.Controls = null

        def updatePreset(): Unit = {
          val preset = ggPreset.selection.item
          if (flow.contents.size > 2) flow.contents.remove(1)
          presetCtl = preset.mkControls()
          flow.contents.insert(1, presetCtl.component)
          Option(SwingUtilities.getWindowAncestor(op.peer)).foreach(_.pack())
        }

        ggPreset.reactions += {
          case SelectionChanged(_) => updatePreset()
        }
        val pane      = new GroupPanel {
          horizontal  = Seq(Par(lbName, lbPreset), Par(ggName, ggPreset))
          vertical    = Seq(Par(Baseline)(lbName, ggName), Par(Baseline)(lbPreset, ggPreset))
        }

        flow.contents += pane
        flow.contents += Swing.VStrut(4)

        updatePreset()

        val objType   = "Global Proc"
        op.title      = s"Add $objType"
        val opRes     = op.show(None)
        import equal.Implicits._
        if (opRes === OptionPane.Result.Ok) {
          val name = ggName.text
          atomic { implicit tx =>
//            ProcActions.insertGlobalRegion(groupH(), name, bus = None)
            import de.sciss.proc.Implicits._
            val obj   = presetCtl.make[T]()
            obj.name  = name
            val group = groupH()
            EditTimeline.addUndo[T](/*objType,*/ group, Span.All, obj)
          }
        }
      }

    private def removeProcs(pvs: Iterable[ProcObjView.Timeline[T]]): Unit =
      if (pvs.nonEmpty) groupHOpt.foreach { groupH =>
        atomic { implicit tx =>
          undoManager.capture("Remove Objects") {
            pvs.iterator.foreach { pv =>
              EditTimeline.unlinkAndRemoveUndo(groupH(), pv.span, pv.obj)
            }
          }
        }
      }

    private def setColumnWidth(tcm: TableColumnModel, idx: Int, w: Int): Unit = {
      val tc = tcm.getColumn(idx)
      tc.setPreferredWidth(w)
      // tc.setMaxWidth      (w)
    }

//    private def getColumnWidth(tcm: TableColumnModel, idx: Int): Int = {
//      val tc = tcm.getColumn(idx)
//      tc.getPreferredWidth
//    }

    def init(timeline: Timeline[T])(implicit tx: T): this.type = {
      for {
        tAttr <- ViewState.map(timeline)
      } {
        stateTable.init(tAttr)
      }
      deferTx(initGUI())
      this
    }

    private def initGUI(): Unit = {
      table             = new Table(tm)

      // XXX TODO: enable the following - but we're loosing default boolean rendering
      //        // Table default has idiotic renderer/editor handling
      //        override lazy val peer: JTable = new JTable /* with Table.JTableMixin */ with SuperMixin
      //      }
      // table.background  = Color.darkGray
      val jt            = table.peer
      // jt.setAutoCreateRowSorter(true)
      jt.setRowSorter(new TableRowSorter[TableModel](tm) {
        // ensure that 'Bus 9-10' comes before 'Bus 11-12' (for example)
        private object StringComparator extends Comparator[String] {
          override def compare(s1: String, s2: String): Int = File.compareName(s1, s2)
        }

        override def getComparator(column: Int): Comparator[_] = column match {
          case 0 => StringComparator
          case _ => super.getComparator(column)
        }
      })

      // jt.putClientProperty("JComponent.sizeVariant", "small")
      // jt.getRowSorter.setSortKeys(...)
      //      val tcm = new DefaultTableColumnModel {
      //
      //      }
      val tcm = jt.getColumnModel
      setColumnWidth(tcm, 0, 55)
      setColumnWidth(tcm, 1, 47)
      setColumnWidth(tcm, 2, 29)
      setColumnWidth(tcm, 3, 43)

      stateTable.initGUI_J(jt)
//      val tabW = 3 +
//        getColumnWidth(tcm, 0) +
//        getColumnWidth(tcm, 1) +
//        getColumnWidth(tcm, 2) +
//        getColumnWidth(tcm, 3)
//      println(s"tabW = $tabW")
//      jt.setPreferredScrollableViewportSize(new Dimension(tabW, 100))

      // ---- drag and drop ----
      jt.setDropMode(DropMode.ON)
      jt.setDragEnabled(true)
      jt.setTransferHandler(new TransferHandler {
        override def getSourceActions(c: JComponent): Int = TransferHandler.LINK

        override def createTransferable(c: JComponent): Transferable = {
          val rows = table.selection.rows
          rows.headOption.map { viewRow =>
            val modelRow = table.viewToModelRow(viewRow)
            val pv = procSeq(modelRow)
            DragAndDrop.Transferable(DnD.flavor)(DnD.GlobalProcDrag(universe, pv.objH))
          } .orNull
        }

        // ---- import ----
        override def canImport(support: TransferSupport): Boolean =
          support.isDataFlavorSupported(ObjView.Flavor)

        override def importData(support: TransferSupport): Boolean =
          support.isDataFlavorSupported(ObjView.Flavor) && {
            Option(jt.getDropLocation).fold(false) { dl =>
              val pv    = procSeq(dl.getRow)
              val drag  = support.getTransferable.getTransferData(ObjView.Flavor)
                .asInstanceOf[ObjView.Drag[T]]
              import de.sciss.equal.Implicits._
              drag.universe === universe && {
                drag.view match {
                  case iv: IntObjView[T] =>
                    atomic { implicit tx =>
                      val objT = iv.obj
                      val intExpr = objT
                      ProcActions.setBus(pv.obj :: Nil, intExpr)
                      true
                    }

//                  case iv: CodeObjView[T] =>
//                    atomic { implicit tx =>
//                      val objT = iv.obj
//                      import Mellite.compiler
//                      ProcActions.setSynthGraph(pv.obj :: Nil, objT)
//                      true
//                    }

                  case _ => false
                }
              }
            }
          }
      })

      // why is this not the default?
      jt.setPreferredScrollableViewportSize(jt.getPreferredSize)

      val scroll    = new ScrollPane(table)
      scroll.peer.putClientProperty("styleId", "undecorated")
      scroll.border = null

      val actionAdd = Action(null)(addItemWithDialog())
      val ggAdd: Button = GUI.addButton(actionAdd, "Add Global Process")

      val actionDelete = Action(null) {
        val pvs = tableSelection()
        removeProcs(pvs)
      }
      val ggDelete: Button = GUI.toolButton(actionDelete, raphael.Shapes.Minus, "Delete Global Process")
      actionDelete.enabled = false

      val actionAttr: Action = Action(null) {
        if (selectionModel.nonEmpty) cursor.step { implicit tx =>
          selectionModel.iterator.foreach { view =>
            AttrMapFrame(view.obj)
          }
        }
      }

      val actionEdit: Action = Action(null) {
        if (selectionModel.nonEmpty) cursor.step { implicit tx =>
          selectionModel.iterator.foreach { view =>
            if (view.isViewable) view.openView(None)  //  /// XXX TODO - find window
          }
        }
      }

      val ggAttr = GUI.toolButton(actionAttr, raphael.Shapes.Wrench, "Attributes Editor")
      actionAttr.enabled = false

      val ggEdit = GUI.toolButton(actionEdit, raphael.Shapes.View, "Proc Editor")
      actionEdit.enabled = false

      table.listenTo(table.selection, table.mouse.clicks)
      table.reactions += {
        case TableRowsSelected(_, _, _) =>
          val newSel  = tableSelection[ObjView[T]]()
          val hasSel  = newSel.nonEmpty
          actionDelete.enabled = hasSel
          actionAttr  .enabled = hasSel
          actionEdit  .enabled = hasSel
          // println(s"Table range = $range")
          selectionModel.iterator.foreach { v =>
            if (!newSel.contains(v)) {
              // println(s"selectionModel -= $v")
              selectionModel -= v
            }
          }
          newSel.foreach { v =>
            if (!selectionModel.contains(v)) {
              // println(s"selectionModel += $v")
              selectionModel += v
            }
          }

        case e: MouseButtonEvent if e.triggersPopup => showPopup(e)
      }

      // Uh, no scala-swing stuff for this.
      table.peer.getTableHeader.addMouseListener(new MouseAdapter {
        private def test(e: AWTMouseEvent): Unit =
          if (e.isPopupTrigger) {
            val s = table.peer.getRowSorter
            if (s != null) s.setSortKeys(null)  // original order, no sorting
          }

        override def mousePressed(e: AWTMouseEvent): Unit = test(e)
        override def mouseClicked(e: AWTMouseEvent): Unit = test(e)
      })

      tlSelModel addListener tlSelListener

      val pBottom = new BoxPanel(Orientation.Vertical)
      if (groupHOpt.isDefined) {
        // only add buttons if group is modifiable
        pBottom.contents += new FlowPanel(ggAdd, ggDelete)
      }
      pBottom.contents += new FlowPanel(ggAttr, ggEdit)

      component = new BorderPanel {
        add(scroll , BorderPanel.Position.Center)
        add(pBottom, BorderPanel.Position.South )
      }
    }

    private def showPopup(e: MouseEvent): Unit = desktop.Window.find(component).foreach { w =>
      val hasGlobal = selectionModel.nonEmpty
      val hasTL     = tlSelModel    .nonEmpty
      if (hasGlobal) {
        import Menu._
        // val itSelect      = Item("select"        )("Select Connected Regions")(selectRegions())
        val itDup         = Item("duplicate"     )("Duplicate"                       )(duplicate(connect = false))
        val itDupC        = Item("duplicate-con" )("Duplicate with Connections"      )(duplicate(connect = true))
        val itConnect     = Item("connect"       )("Connect to Selected Regions"     )(connectToSelectedRegions())
        val itDisconnect  = Item("disconnect"    )("Disconnect from Selected Regions")(disconnectFromSelectedRegions())
        val itDisconnectA = Item("disconnect-all")("Disconnect from All Regions"     )(disconnectFromAllRegions())
        if (groupHOpt.isEmpty) {
          itDup .disable()
          itDupC.disable()
        }
        if (!hasTL) {
          itConnect   .disable()
          itDisconnect.disable()
        }

        val pop = Popup().add(itDup).add(itDupC).add(Line).add(itConnect).add(itDisconnect).add(itDisconnectA)
        pop.create(w).show(component, e.point.x, e.point.y)
      }
    }

//    private def selectRegions(): Unit = {
//      val itGlob = selectionModel.iterator
//      cursor.step { implicit tx =>
//        val scans = itGlob.flatMap { inView =>
//          inView.obj.scans.get("in")
//        }
//        tlSelModel.
//      }
//    }

    private def duplicate(connect: Boolean): Unit = groupHOpt.foreach { groupH =>
      val itGlob = selectionModel.iterator
      cursor.step { implicit tx =>
        val tl = groupH()
        undoManager.capture("Duplicate Global Procs") {
          itGlob.foreach { inView =>
            val inObj   = inView.obj
  //          val span    = inView.span   // not necessary to copy
            val span    = SpanLikeObj.newConst[T](Span.all)
            val outObj  = ProcActions.copy[T](inObj, connectInput = connect)
            EditTimeline.addUndo(/*"Insert Global Proc",*/ tl, span, outObj)
          }
        }
      }
    }

    private def connectToSelectedRegions(): Unit = {
      val seqGlob = selectionModel.iterator.toSeq
      val seqTL   = tlSelModel    .iterator.toSeq
      val plGlob  = seqGlob.size > 1
      val plTL    = seqTL  .size > 1
      cursor.step { implicit tx =>
        val editName = s"Connect Global ${if (plGlob) "Procs" else "Proc"} to Selected ${if (plTL) "Regions" else "Region"}"
        undoManager.capture(editName) {
          for {
            outView <- seqTL
            inView  <- seqGlob
            in      <- inView .obj match { case p: Proc[T] => Some(p); case _ => None }
            out     <- outView.obj match { case p: Proc[T] => Some(p); case _ => None } // Proc.unapply(outView.obj)
            source  <- out.outputs.get(Proc.mainOut)
            if Edits.findLink(out = out, in = in).isEmpty
          } Edits.addLink(source = source, sink = in, key = Proc.mainIn)
        }
      }
    }

    private def disconnectFromSelectedRegions(): Unit = {
      val seqGlob = selectionModel.iterator.toSeq
      val seqTL   = tlSelModel    .iterator.toSeq
      val plGlob  = seqGlob.size > 1
      val plTL    = seqTL  .size > 1
      cursor.step { implicit tx =>
        val editName = s"Disconnect Global ${if (plGlob) "Procs" else "Proc"} from Selected ${if (plTL) "Regions" else "Region"}"
        undoManager.capture(editName) {
          for {
            outView <- seqTL
            inView  <- seqGlob
            in      <- inView .obj match { case p: Proc[T] => Some(p); case _ => None } // Proc.unapply(outView.obj)
            out     <- outView.obj match { case p: Proc[T] => Some(p); case _ => None } // Proc.unapply(outView.obj)
            link    <- Edits.findLink(out = out, in = in)
          } Edits.removeLink(link)
        }
      }
    }

    private def removeInputs(in: Obj[T])(implicit tx: T): Boolean =
      if (!in.attr.contains(Proc.mainIn)) false else {
        EditAttrMap.removeUndo(/*name = "Input",*/ in.attr, key = Proc.mainIn)
        true
      }

    private def disconnectFromAllRegions(): Unit = {
      val seqGlob = selectionModel.iterator.toList
      val plGlob  = seqGlob.size > 1
      cursor.step { implicit tx =>
        val editName = s"Disconnect Global ${if (plGlob) "Procs" else "Proc"} from All Regions"
        undoManager.capture(editName) {
          seqGlob.foreach { inView =>
            val in = inView.obj
            removeInputs(in)
          }
        }
      }
    }

    override def dispose()(implicit tx: T): Unit = deferTx {
      tlSelModel removeListener tlSelListener
    }

    override def add(proc: ObjView[T]): Unit = proc match {
      case pv: ProcObjView.Timeline[T] =>
        val row   = procSeq.size
        procSeq :+= pv
        tm.fireTableRowsInserted(row, row)

      case _ =>
    }

    override def remove(proc: ObjView[T]): Unit = {
      val row = procSeq.indexOf(proc)
      if (row >= 0) {
        procSeq = procSeq.patch(row, Vec.empty, 1)
        tm.fireTableRowsDeleted(row, row)
      }
    }

    override def iterator: Iterator[ProcObjView.Timeline[T]] = procSeq.iterator

    override def updated(proc: ObjView[T]): Unit = {
      val row = procSeq.indexOf(proc)
      if (row >= 0) {
        tm.fireTableRowsUpdated(row, row)
      }
    }
  }
}