/*
 *  LinkTargetOutput.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite.impl.proc

import de.sciss.lucre.edit.{EditAttrMap, UndoManager}
import de.sciss.lucre.synth.Txn
import de.sciss.mellite.impl.proc.ProcObjView.LinkTarget

final class LinkTargetOutput[T <: Txn[T]](val attr: InputAttrOutput[T])
  extends LinkTarget[T] {

  override def toString: String = s"LinkTargetOutput($attr)@${hashCode.toHexString}"

  override def remove()(implicit tx: T, undoManager: UndoManager[T]): Boolean = {
    val obj = attr.parent.obj
    EditAttrMap.remove(/*"Output",*/ obj.attr, key = attr.key)
    true
  }
}

