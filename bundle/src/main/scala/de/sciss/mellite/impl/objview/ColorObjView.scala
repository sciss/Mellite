/*
 *  ColorObjView.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite.impl.objview

import de.sciss.icons.raphael
import de.sciss.lucre.expr.CellView
import de.sciss.lucre.expr.graph.Ex
import de.sciss.lucre.swing.LucreSwing.deferTx
import de.sciss.lucre.swing.edit.EditVar
import de.sciss.lucre.swing.impl.ComponentHolder
import de.sciss.lucre.swing.{View, Window}
import de.sciss.lucre.synth.Txn
import de.sciss.lucre.{Expr, Obj, Source, Txn => LTxn}
import de.sciss.mellite.impl.component.PaintIcon
import de.sciss.mellite.impl.objview.ObjViewImpl.raphaelIcon
import de.sciss.mellite.impl.{ObjViewCmdLineParser, WorkspaceWindowImpl}
import de.sciss.mellite.{MessageException, ObjListView, ObjView, UniverseHandler, UniverseObjView, ViewState, WorkspaceWindow}
import de.sciss.proc.{Code, Color, Universe}
import de.sciss.{desktop, numbers}
import org.rogach.scallop

import javax.swing.Icon
import scala.swing.{Action, BorderPanel, Button, ColorChooser, Component, FlowPanel, GridPanel, Label, Swing}
import scala.util.{Failure, Success, Try}

object ColorObjView extends ObjListView.Factory with ProgramSupport {
  type E[~ <: LTxn[~]] = Color.Obj[~]
  val icon          : Icon      = raphaelIcon(raphael.Shapes.Paint)
  val prefix        : String    = "Color"
  def humanName     : String    = prefix
  def category      : String    = ObjView.categOrganization
  type Elem                     = Color

  def tpe     : Expr.Type[Elem, E]          = Color.Obj
  val codeType: Code.TypeT[Unit, Ex[Elem]]  = Code.Program.Color

  def mkListView[T <: Txn[T]](obj: Color.Obj[T])(implicit tx: T): ColorObjView[T] with ObjListView[T] = {
    val ex          = obj
    val value       = ex.value
    val isEditable  = Expr.isVar(ex)
    val isProgram   = Expr.isProgram(ex)
    val isViewable  = isEditable || isProgram // || tx.isInstanceOf[Confluent.Txn]
    new ListImpl[T](tx.newHandle(obj), value, isEditable0 = isEditable, isProgram = isProgram,
      isViewable = isViewable).init(obj)
  }

  override def initMakeDialog[T <: Txn[T]](window: Option[desktop.Window])
                                          (implicit universe: Universe[T]): MakeResult[T] = {
    val (ggValue, ggChooser) = mkColorEditor()
    val codeValue0  = "Color()"
    showMakeDialog(ggValue = ggValue, codeValue0 = codeValue0, prefix = prefix,
      window = window)(fromAWT(ggChooser.color))
  }

  private def mkColorEditor(): (Component, ColorChooser) = {
    val chooser = new ColorChooser()
    val bPredef = Color.Palette.map { colr =>
      val action: Action = new Action(null /* colr.name */) {
        private val awtColor = toAWT(colr)
        icon = new PaintIcon(awtColor, 32, 32)
        def apply(): Unit = chooser.color = awtColor
      }
      val b = new Button(action)
      // b.horizontalAlignment = Alignment.Left
      b.focusable = false
      b
    }
    val pPredef = new GridPanel(4, 4)
    pPredef.contents ++= bPredef
    val panel = new BorderPanel {
      add(pPredef, BorderPanel.Position.West  )
      add(chooser, BorderPanel.Position.Center)
    }
    (panel, chooser)
  }

  def toAWT(c: Color): java.awt.Color = new java.awt.Color(c.argb)
  def fromAWT(c: java.awt.Color): Color = {
    val argb = c.getRGB
    Color.Palette.find(_.argb == argb).getOrElse(Color.User(argb))
  }

  override protected def scallopValueConverter: scallop.ValueConverter[Color] = scallop.singleArgConverter { s =>
    parseString(s).get
  }

  override def initMakeCmdLine[T <: Txn[T]](args: List[String])(implicit universe: Universe[T]): MakeResult[T] = {
    object p extends ObjViewCmdLineParser[Config[T]](this, args /*args1*/) {
      val const: Opt[Boolean] = opt(descr = s"Make constant instead of variable")

      val value: Opt[Either[Elem, codeType.Repr]] = trailArg(
        descr = s"Initial $prefix value (0-${Color.Palette.size - 1}, #rrggbb, red, rgb(), hsl(), ...)"
      )
    }
    p.parseFut {
      prepareConfig[T](p.name(), p.value(), p.const())
    }
  }

  private lazy val cssPredefined: Map[String, Int] = Map(
    "AliceBlue"         -> 0xF0F8FF, "AntiqueWhite"      -> 0xFAEBD7, "Aqua"              -> 0x00FFFF,
    "Aquamarine"        -> 0x7FFFD4, "Azure"             -> 0xF0FFFF, "Beige"             -> 0xF5F5DC,
    "Bisque"            -> 0xFFE4C4, "Black"             -> 0x000000, "BlanchedAlmond"    -> 0xFFEBCD,
    "Blue"              -> 0x0000FF, "BlueViolet"        -> 0x8A2BE2, "Brown"             -> 0xA52A2A,
    "BurlyWood"         -> 0xDEB887, "CadetBlue"         -> 0x5F9EA0, "Chartreuse"        -> 0x7FFF00,
    "Chocolate"         -> 0xD2691E, "Coral"             -> 0xFF7F50, "CornflowerBlue"    -> 0x6495ED,
    "Cornsilk"          -> 0xFFF8DC, "Crimson"           -> 0xDC143C, "Cyan"  	          -> 0x00FFFF,
    "DarkBlue"          -> 0x00008B, "DarkCyan"          -> 0x008B8B, "DarkGoldenRod"     -> 0xB8860B,
    "DarkGray"          -> 0xA9A9A9, "DarkGrey"          -> 0xA9A9A9, "DarkGreen"         -> 0x006400,
    "DarkKhaki"         -> 0xBDB76B, "DarkMagenta"       -> 0x8B008B, "DarkOliveGreen"    -> 0x556B2F,
    "DarkOrange"        -> 0xFF8C00, "DarkOrchid"        -> 0x9932CC, "DarkRed"           -> 0x8B0000,
    "DarkSalmon"        -> 0xE9967A, "DarkSeaGreen"      -> 0x8FBC8F, "DarkSlateBlue"     -> 0x483D8B,
    "DarkSlateGray"     -> 0x2F4F4F, "DarkSlateGrey"     -> 0x2F4F4F, "DarkTurquoise"     -> 0x00CED1,
    "DarkViolet"        -> 0x9400D3, "DeepPink"          -> 0xFF1493, "DeepSkyBlue"       -> 0x00BFFF,
    "DimGray"           -> 0x696969, "DimGrey"           -> 0x696969, "DodgerBlue"        -> 0x1E90FF,
    "FireBrick"         -> 0xB22222, "FloralWhite"       -> 0xFFFAF0, "ForestGreen"       -> 0x228B22,
    "Fuchsia"           -> 0xFF00FF, "Gainsboro"         -> 0xDCDCDC, "GhostWhite"        -> 0xF8F8FF,
    "Gold"              -> 0xFFD700, "GoldenRod"         -> 0xDAA520, "Gray"              -> 0x808080,
    "Grey"              -> 0x808080, "Green"             -> 0x008000, "GreenYellow"       -> 0xADFF2F,
    "HoneyDew"          -> 0xF0FFF0, "HotPink"           -> 0xFF69B4, "IndianRed"         -> 0xCD5C5C,
    "Indigo"            -> 0x4B0082, "Ivory"             -> 0xFFFFF0, "Khaki"             -> 0xF0E68C,
    "Lavender"          -> 0xE6E6FA, "LavenderBlush"     -> 0xFFF0F5, "LawnGreen"         -> 0x7CFC00,
    "LemonChiffon"      -> 0xFFFACD, "LightBlue"         -> 0xADD8E6, "LightCoral"        -> 0xF08080,
    "LightCyan"         -> 0xE0FFFF, "LightGoldenRodYellow" -> 0xFAFAD2, "LightGray"      -> 0xD3D3D3,
    "LightGrey"         -> 0xD3D3D3, "LightGreen"        -> 0x90EE90, "LightPink"         -> 0xFFB6C1,
    "LightSalmon"       -> 0xFFA07A, "LightSeaGreen"     -> 0x20B2AA, "LightSkyBlue"      -> 0x87CEFA,
    "LightSlateGray"    -> 0x778899, "LightSlateGrey"    -> 0x778899, "LightSteelBlue"    -> 0xB0C4DE,
    "LightYellow"       -> 0xFFFFE0, "Lime"              -> 0x00FF00, "LimeGreen"         -> 0x32CD32,
    "Linen"             -> 0xFAF0E6, "Magenta"           -> 0xFF00FF, "Maroon"            -> 0x800000,
    "MediumAquaMarine"  -> 0x66CDAA, "MediumBlue"        -> 0x0000CD, "MediumOrchid"      -> 0xBA55D3,
    "MediumPurple"      -> 0x9370DB, "MediumSeaGreen"    -> 0x3CB371, "MediumSlateBlue"   -> 0x7B68EE,
    "MediumSpringGreen" -> 0x00FA9A, "MediumTurquoise"   -> 0x48D1CC, "MediumVioletRed"   -> 0xC71585,
    "MidnightBlue"      -> 0x191970, "MintCream"         -> 0xF5FFFA, "MistyRose"         -> 0xFFE4E1,
    "Moccasin"          -> 0xFFE4B5, "NavajoWhite"       -> 0xFFDEAD, "Navy"              -> 0x000080,
    "OldLace"           -> 0xFDF5E6, "Olive"             -> 0x808000, "OliveDrab"         -> 0x6B8E23,
    "Orange"            -> 0xFFA500, "OrangeRed"         -> 0xFF4500, "Orchid"            -> 0xDA70D6,
    "PaleGoldenRod"     -> 0xEEE8AA, "PaleGreen"         -> 0x98FB98, "PaleTurquoise"     -> 0xAFEEEE,
    "PaleVioletRed"     -> 0xDB7093, "PapayaWhip"        -> 0xFFEFD5, "PeachPuff"         -> 0xFFDAB9,
    "Peru"              -> 0xCD853F, "Pink"              -> 0xFFC0CB, "Plum"              -> 0xDDA0DD,
    "PowderBlue"        -> 0xB0E0E6, "Purple"            -> 0x800080, "RebeccaPurple"     -> 0x663399,
    "Red"               -> 0xFF0000, "RosyBrown"         -> 0xBC8F8F, "RoyalBlue"         -> 0x4169E1,
    "SaddleBrown"       -> 0x8B4513, "Salmon"            -> 0xFA8072, "SandyBrown"        -> 0xF4A460,
    "SeaGreen"          -> 0x2E8B57, "SeaShell"          -> 0xFFF5EE, "Sienna"            -> 0xA0522D,
    "Silver"            -> 0xC0C0C0, "SkyBlue"           -> 0x87CEEB, "SlateBlue"         -> 0x6A5ACD,
    "SlateGray"         -> 0x708090, "SlateGrey"         -> 0x708090, "Snow"              -> 0xFFFAFA,
    "SpringGreen"       -> 0x00FF7F, "SteelBlue"         -> 0x4682B4, "Tan"               -> 0xD2B48C,
    "Teal"              -> 0x008080, "Thistle"           -> 0xD8BFD8, "Tomato"            -> 0xFF6347,
    "Turquoise"         -> 0x40E0D0, "Violet"            -> 0xEE82EE, "Wheat"             -> 0xF5DEB3,
    "White"             -> 0xFFFFFF, "WhiteSmoke"        -> 0xF5F5F5, "Yellow"            -> 0xFFFF00,
    "YellowGreen"       -> 0x9ACD32
  ) .map { case (k, v) => (k.toLowerCase, v) }

  // supports some of the CSS syntax,
  // see https://www.w3schools.com/CSSref/css_colors_legal.asp
  private def parseString(s: String): Try[Color] = {
    def fail(msg: String) =
      Failure(MessageException(msg))

    val t     = s.trim.toLowerCase
    val i     = t.indexOf('(')
    val isFun = i >= 0
    if (isFun) {
      val j = t.indexOf(')')
      if (j != t.length - 1) fail(s"Unbalanced parentheses")
      else {
        val args: Array[String] = t.substring(i + 1, j).split(",").map(_.trim)
        val numArgs = args.length
        val funName = t.substring(0, i).trim

        def requireNumArgs(exp: Int)(body: => Try[Color]): Try[Color] =
          if (numArgs == exp) body else
            fail(s"Color function '$funName' requires $exp arguments, not $numArgs")

        import numbers.Implicits._

        def parseIntPercent(x: String): Try[Int] = Try {
          if (x.endsWith("%")) {
            x.substring(0, x.length - 1).toInt.clip(0, 100).linLin(0, 100, 0, 255).toInt
          } else {
            x.toInt.clip(0, 255)
          }
        }

        def parseFraction(x: String): Try[Int] = Try {
          (x.toDouble.clip(0, 1) * 255).toInt
        }

        def parseHue(x: String): Try[Float] = Try {
          x.toInt.wrap(0, 360) / 360f
        }

        def parsePercent(x: String): Try[Float] = Try {
          x.substring(0, x.length - 1).toInt.clip(0, 100) / 100f
        }

        funName match {
          case "rgb"  => requireNumArgs(3) {
              for {
                r <- parseIntPercent(args(0))
                g <- parseIntPercent(args(1))
                b <- parseIntPercent(args(2))
              } yield Color.User(0xFF000000 | (r << 16) | (g << 8) | b)
            }
          case "rgba" => requireNumArgs(4) {
            for {
              r <- parseIntPercent(args(0))
              g <- parseIntPercent(args(1))
              b <- parseIntPercent(args(2))
              a <- parseFraction  (args(3))
            } yield Color.User((a << 24) | (r << 16) | (g << 8) | b)
          }
          case "hsl"  => requireNumArgs(3) {
            for {
              h <- parseHue       (args(0))
              s <- parsePercent   (args(1))
              b <- parsePercent   (args(2))
            } yield {
              val awt = java.awt.Color.getHSBColor(h, s, b)
              fromAWT(awt)
            }
          }
          case "hsla" => requireNumArgs(4) {
            for {
              h <- parseHue       (args(0))
              s <- parsePercent   (args(1))
              b <- parsePercent   (args(2))
              a <- parseFraction  (args(3))
            } yield {
              val awt0  = java.awt.Color.getHSBColor(h, s, b)
              val rgba  = (awt0.getRGB & 0x00FFFFFF) | (a << 24)
              Color.User(rgba)
            }
          }
          case _ =>
            fail(s"Unsupported color function '$funName'")
        }
      }

    } else if (t.startsWith("#")) {  // hex
      Try(java.lang.Integer.parseInt(t.substring(1), 16)).map(hex =>
        Color.User(0xFF000000 | hex)
      )

    } else if (t.length > 0 && t.charAt(0).isDigit) { // predefined
      Try(t.toInt).flatMap { id =>
        if (id >= 0 && id < Color.Palette.size) {
          Success(Color.Palette(id))
        } else {
          fail(s"Unknown color name '$t'")
        }
      }
    } else {  // assume predefined CSS name
      cssPredefined.get(t) match {
        case Some(hex)  => Success(Color.User(0xFF000000 | hex))
        case None       => fail(s"Unknown color name '$t'")
      }
    }
  }

  private object ColorFrame extends WorkspaceWindow.Key {
    type Repr[T <: LTxn[T]] = ColorFrame[T]
  }

  private trait ColorFrame[T <: LTxn[T]] extends WorkspaceWindow[T] {
    type Repr[~ <: LTxn[~]] = ColorFrame[~]
  }

  private final class ListImpl[T <: Txn[T]](val objH: Source[T, Color.Obj[T]], var value: Color,
                                            isEditable0: Boolean, val isProgram: Boolean,
                                            val isViewable: Boolean
                                           )
    extends ColorObjView[T] with ListBase[T] with ObjListViewImpl.SimpleExpr[T, Color, Color.Obj] { listView =>

    override def isListCellEditable = false    // not until we have proper editing components

    override def configureListCellRenderer(label: Label): Component = {
      // renderers are used for "stamping", so we can reuse a single object.
      label.text = if (isProgram) "=" else null
      label.icon = ListIcon
      ListIcon.paint = toAWT(value)
      label
    }

    override def convertEditValue(v: Any): Option[Color] = v match {
      case c: Color => Some(c)
      case _        => None
    }

    override protected def openValueView(parent: Option[Window[T]])(implicit tx: T,
                                                                    handler: UniverseHandler[T]): Option[Window[T]] = {
      val _obj = obj
      val res = handler(_obj, ColorFrame) {
        newFrame(_obj, parent)
      }
      Some(res)
    }
  }

  private def newFrame[T <: Txn[T]](obj: Color.Obj[T], parent: Option[Window[T]])
                   (implicit tx: T, _handler: UniverseHandler[T]): ColorFrame[T] = {
    val name  = CellView.name(obj)
    val res   = new FrameImpl[T](tx.newHandle(obj), obj.value, parent, tx)
    res.init().setTitle(name)
  }

  private final class FrameImpl[T <: Txn[T]](objH: Source[T, Color.Obj[T]], value0: Color,
                                             parent: Option[Window[T]], tx0: T)
                                            (implicit val handler: UniverseHandler[T])
    extends ColorFrame[T] with WorkspaceWindowImpl[T] { self =>

    override def key: Key = ColorFrame

    override def newWindow()(implicit tx: T): ColorFrame[T] =
      newFrame(objH(), parent)

    override protected def resizable: Boolean = false

    val view: UniverseObjView[T] = new UniverseObjView[T] with ComponentHolder[Component] {
      type C = Component
      val universe: Universe[T] = handler.universe

      deferTx {
        val (compColor, chooser) = mkColorEditor()
        chooser.color = toAWT(value0)
        val ggCancel = Button("Cancel") {
          closeMe() // self.handleClose()
        }

        def apply(): Unit = {
          val colr = fromAWT(chooser.color)
          cursor.step { implicit tx =>
            objH() match {
              case Color.Obj.Var(vr) =>
                def edit(): Unit =
                  EditVar.expr[T, Color, Color.Obj]("Change Color", vr, Color.Obj.newConst[T](colr))

                parent.foreach { p =>
                  p.view match {
                    case e: View.Editable[T] => e.undoManager.use(edit())
                    case _ => edit()
                  }
                }
              case _ => ()
            }
          }
        }

        val ggOk = Button("Ok") {
          apply()
          closeMe() // self.handleClose()
        }
        val ggApply = Button("Apply") {
          apply()
        }
        val pane = new BorderPanel {
          add(compColor, BorderPanel.Position.Center)
          add(new FlowPanel(ggOk, ggApply, Swing.HStrut(8), ggCancel), BorderPanel.Position.South)
        }

        component = pane
      } (tx0)

      override def obj(implicit tx: T): Obj[T] = objH()

      override def viewState: Set[ViewState] = Set.empty

      def dispose()(implicit tx: T): Unit = ()
    }

    def closeMe(): Unit = {
      import view.universe.cursor
      cursor.step { implicit tx => self.dispose() }
    }
  }

  private val ListIcon = new PaintIcon(java.awt.Color.black, 48, 16)
}
trait ColorObjView[T <: LTxn[T]] extends ObjView[T] {
  type Repr = Color.Obj[T]
}