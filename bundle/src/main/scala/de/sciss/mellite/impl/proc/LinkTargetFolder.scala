/*
 *  LinkTargetFolder.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite.impl.proc

import de.sciss.lucre.edit.{EditFolder, UndoManager}
import de.sciss.lucre.synth.Txn
import de.sciss.lucre.{Obj, Source}
import de.sciss.mellite.impl.proc.ProcObjView.LinkTarget

final class LinkTargetFolder[T <: Txn[T]](val attr: InputAttrFolder[T],
                                          objH : Source[T, Obj[T]])
  extends LinkTarget[T] {

  override def toString: String = s"LinkTargetFolder($attr)@${hashCode.toHexString}"

  override def remove()(implicit tx: T, undoManager: UndoManager[T]): Boolean = {
    val f     = attr.folder
    val obj   = objH()
    val index = f.indexOf(obj)
    if (index < 0) false else {
      EditFolder.removeUndo(/*"Proc",*/ f, /*index = index,*/ child = obj)
      true
    }
  }
}
