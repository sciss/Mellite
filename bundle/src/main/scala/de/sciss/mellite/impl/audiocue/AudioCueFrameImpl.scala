/*
 *  AudioCueFrameImpl.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite.impl.audiocue

import de.sciss.asyncfile.Ops._
import de.sciss.file._
import de.sciss.lucre.Expr
import de.sciss.lucre.expr.CellView
import de.sciss.lucre.synth.Txn
import de.sciss.mellite.impl.WorkspaceWindowImpl
import de.sciss.mellite.{AudioCueFrame, AudioCueView, UniverseHandler}
import de.sciss.proc.AudioCue

import java.net.URI
import scala.swing.Action
import scala.util.Try

object AudioCueFrameImpl {
  def apply[T <: Txn[T]](obj: AudioCue.Obj[T])
                        (implicit tx: T, handler: UniverseHandler[T]): AudioCueFrame[T] =
    handler(obj, AudioCueFrame) {
      val afv       = AudioCueView(obj)
      val name0     = CellView.name(obj)
      val file      = obj.value.artifact
      val fileName  = file.base
      import de.sciss.equal.Implicits._
      val name      = name0.map { n =>
        if (n === fileName) n else s"$n- $fileName"
      }

      val res = new Impl[T](view = afv, uri = file)
      res.init().setTitle(name)
    }

  private def newInstance[T <: Txn[T]](obj: AudioCue.Obj[T])
                        (implicit tx: T, handler: UniverseHandler[T]): AudioCueFrame[T] = {
    val afv       = AudioCueView(obj)
    val name0     = CellView.name(obj)
    val file      = obj.value.artifact
    val fileName  = file.base
    import de.sciss.equal.Implicits._
    val name      = name0.map { n =>
      if (n === fileName) n else s"$n- $fileName"
    }

    val res = new Impl[T](view = afv, uri = file)
    res.init().setTitle(name)
  }

  private final class Impl[T <: Txn[T]](val view: AudioCueView[T], uri: URI)
                                       (implicit val handler: UniverseHandler[T])
    extends WorkspaceWindowImpl[T] with AudioCueFrame[T] {

    override protected def initGUI(): Unit = {
      super.initGUI()
      val fileOpt = Try(new File(uri)).toOption
      windowFile  = fileOpt
      bindMenus("actions.debug-print" -> Action(null) {
        val s = view.cursor.step { implicit tx =>
          def loop(in: AudioCue.Obj[T]): String = in match {
            case AudioCue.Obj.Var(x)               => s"Var(${loop(x())})"
            case Expr.Const(x)                     => s"Const($x)"
            case x: AudioCue.Obj.Shift         [T] => s"Shift(${loop(x.peer)}, ${x.amount.value})"
            case x: AudioCue.Obj.ReplaceOffset [T] => s"ReplaceOffset(${loop(x.peer)}, ${x.offset.value})"
            case x: AudioCue.Obj.Apply         [T] => s"Apply(${x.artifact.value}, ${x.spec}, ${x.offset.value}, ${x.gain.value})"
            case _ => "(unknown)"
          }
          loop(view.obj)
        }
        println(s)
      })
    }

    override def newWindow()(implicit tx: T): AudioCueFrame[T] =
      newInstance(view.obj)
  }
}