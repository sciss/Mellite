/*
 *  ProgramSupport.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite.impl.objview

import de.sciss.desktop
import de.sciss.lucre.expr.graph.{Const, Ex}
import de.sciss.lucre.swing.Window
import de.sciss.lucre.synth.Txn
import de.sciss.lucre.synth.Executor.executionContext
import de.sciss.lucre.{Expr, Obj, Txn => LTxn}
import de.sciss.mellite.{CodeFrame, ObjListView, ObjView, UniverseHandler}
import de.sciss.mellite.impl.ObjViewCmdLineParser
import de.sciss.proc.{Code, Universe}
import de.sciss.proc.Implicits._
import de.sciss.processor.Processor.Aborted
import de.sciss.swingplus.{ComboBox, GroupPanel, OverlayPanel}
import org.rogach.scallop

import scala.concurrent.Future
import scala.swing.Swing.EmptyIcon
import scala.swing.event.SelectionChanged
import scala.swing.{Alignment, Component, Dialog, Label, TextField}

/** Mixin for expr based obj view factories supporting program code */
trait ProgramSupport extends ObjView.Factory {
  companion =>

  type Elem
  val codeType: Code.TypeT[Unit, Ex[Elem]]

  type E[~ <: LTxn[~]] <: Expr[~, Elem]

  override def tpe: Expr.Type[Elem, E]

  override def canMakeObj: Boolean = true

  final case class Config[T <: LTxn[T]](name: String = prefix, value: Either[Elem, (Code, Ex[Elem])],
                                        const: Boolean = false)

  protected def scallopValueConverter: scallop.ValueConverter[Elem]

  implicit protected object codeValueConverter extends scallop.ValueConverter[Either[Elem, codeType.Repr]] {
    override val argType: scallop.ArgType.V = scallop.ArgType.SINGLE

    override def parse(s: List[(String, List[String])]): Either[String, Option[Either[Elem, codeType.Repr]]] =
      s match {
        case (_, s1 :: Nil) :: Nil =>
          if (s1.startsWith("=")) {
            val s2 = s1.substring(1).trim
            val c: codeType.Repr = codeType.mkCode(s2) // IntelliJ highlight error
            Right(Some(Right(c)))
          } else {
            scallopValueConverter.parse(s).map(_.map(Left(_)))
          }

        case Nil => Right(None)
        case _ => Left("Provide one value")
      }
  }

  override def initMakeCmdLine[T <: Txn[T]](args: List[String])(implicit universe: Universe[T]): MakeResult[T] = {
    object p extends ObjViewCmdLineParser[Config[T]](this, args /*args1*/) {
      val const: Opt[Boolean] = opt(descr = s"Make constant instead of variable")

      val value: Opt[Either[Elem, codeType.Repr]] = trailArg(descr = s"Initial $prefix value")
    }
    p.parseFut {
      prepareConfig[T](p.name(), p.value(), p.const())
    }
  }

  protected def showMakeDialog[T <: Txn[T]](ggValue: Component, codeValue0: String,
                                          prefix: String, window: Option[desktop.Window])
                                         (prepareValue: => Elem): MakeResult[T] = {
    val ggName      = new TextField(10)
    ggName.text     = prefix
    val lbName      = new Label(   "Name:", EmptyIcon, Alignment.Right)
    val ggValueType = new ComboBox(Seq("Value:", "Program:"))
    val ggProgram   = new TextField(s"In($codeValue0)", 10)
    ggProgram.visible = false
    val ggOverlay   = new OverlayPanel
    ggOverlay.contents ++= ggValue :: ggProgram :: Nil
    ggValueType.selection.reactions += {
      case SelectionChanged(_) =>
        val isValue = ggValueType.selection.index == 0
        ggValue   .visible = isValue
        ggProgram .visible = !isValue
        (if (isValue) ggValue else ggProgram).requestFocus()
    }

    val box = new GroupPanel {
      horizontal  = Seq(Par(Trailing)(lbName, ggValueType ), Par          (ggName     , ggOverlay))
      vertical    = Seq(Par(Baseline)(lbName, ggName      ), Par(Baseline)(ggValueType, ggOverlay))
    }

    val pane = desktop.OptionPane.confirmation(box, optionType = Dialog.Options.OkCancel,
      messageType = Dialog.Message.Question, focus = Some(ggValue))
    pane.title  = s"New $prefix"
    val res = pane.show(window)
    if (res == Dialog.Result.Ok) {
      val name    = ggName.text
      val valueIn = if (ggValueType.selection.index == 0) {
        Left(prepareValue)
      } else {
        Right(codeType.mkCode(ggProgram.text))
      }
      prepareConfig[T](name, valueIn) // IntelliJ highlight error
    } else {
      Future.failed(Aborted())
    }
  }

  def makeObj[T <: Txn[T]](config: Config[T])(implicit tx: T): List[Obj[T]] = {
    import config._
    val obj = value match {
      case Left(c) =>
        val obj0 = tpe.newConst[T](c)
        if (const) obj0 else tpe.newVar(obj0)
      case Right((code, tree)) =>
        val obj0 = tpe.newProgram[T](tree)
        obj0.attr.put(Code.attrSource, Code.Obj.newVar[T](code))
        obj0
    }
    if (name.nonEmpty) obj.name = name
    obj :: Nil
  }

  protected def prepareConfig[T <: Txn[T]](name: String, valueIn: Either[Elem, codeType.Repr],
                                         const: Boolean = false): MakeResult[T] = {
    val valueFut: Future[Either[Elem, (Code, Ex[Elem])]] = valueIn match {
      case Left(c) => Future.successful(Left(c))
      case Right(code) =>
        import de.sciss.mellite.Mellite.compiler
        Code.future {
          val c = code.execute(())
          c match {
            case Const(c) => Left(c)
            case tree     => Right((code, tree))
          }
        }
    }
    valueFut.map { value =>
      Config(name = name, value = value, const = const)
    }
  }

  protected trait StringRenderer {
    def value: Elem
    protected def isProgram: Boolean

    def configureListCellRenderer(label: Label): Component = {
      val valueS = value.toString
      label.text = if (isProgram) s"= $valueS" else valueS
      label
    }
  }

  protected trait Base[T <: Txn[T]] extends ObjView[T]
    with ObjViewImpl.Impl[T]
    with ObjViewImpl.ExprLike[T, Elem, E] {

    override type Repr = E[T]

    def factory: ObjView.Factory = companion

    implicit val exprType: Expr.Type[Elem, E] = companion.tpe

    def expr(implicit tx: T): E[T] = obj
  }

  protected trait ListBase[T <: Txn[T]] extends ObjListView[T]
    with Base[T]
    with ObjListViewImpl.ExprLike[T, Elem, E] {

    protected def isProgram: Boolean

    protected def openValueView(parent: Option[Window[T]])
                               (implicit tx: T, handler: UniverseHandler[T]): Option[Window[T]] =
      openConfluentView(parent)

    override def openView(parent: Option[Window[T]])(implicit tx: T, handler: UniverseHandler[T]): Option[Window[T]] =
      if (isProgram) {
        import de.sciss.mellite.Mellite.compiler
        obj match {
          case exprType.Program(p) =>
            val frame = CodeFrame.program[T, Elem, E](p)
            Some(frame)

          case _ => None
        }
      } else {
        openValueView(parent)
      }
  }
}
