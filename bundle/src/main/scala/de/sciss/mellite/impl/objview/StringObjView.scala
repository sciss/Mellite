/*
 *  StringObjView.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite.impl.objview

import de.sciss.desktop
import de.sciss.icons.raphael
import de.sciss.lucre.expr.graph.Ex
import de.sciss.lucre.synth.Txn
import de.sciss.lucre.{Expr, Source, StringObj, Txn => LTxn}
import de.sciss.mellite.impl.objview.ObjViewImpl.raphaelIcon
import de.sciss.mellite.{ObjListView, ObjView}
import de.sciss.proc.{Code, Confluent, Universe}
import org.rogach.scallop

import javax.swing.Icon
import scala.swing.TextField

object StringObjView extends ObjListView.Factory with ProgramSupport {
  type E[~ <: LTxn[~]] = StringObj[~]
  val icon          : Icon    = raphaelIcon(raphael.Shapes.Font)
  val prefix        : String  = "String"
  def humanName     : String  = prefix
  def category      : String  = ObjView.categPrimitives
  type Elem                   = String

  def tpe     : Expr.Type[Elem, E]          = StringObj
  val codeType: Code.TypeT[Unit, Ex[Elem]]  = Code.Program.String

  override protected def scallopValueConverter: scallop.ValueConverter[Elem] = scallop.stringConverter

  def mkListView[T <: Txn[T]](obj: StringObj[T])(implicit tx: T): StringObjView[T] with ObjListView[T] = {
    val ex          = obj
    val value       = ex.value
    val isEditable  = Expr.isVar(ex)
    val isProgram   = Expr.isProgram(ex)
    val isViewable = isProgram || tx.isInstanceOf[Confluent.Txn]
    new Impl[T](tx.newHandle(obj), value, isListCellEditable = isEditable, isProgram = isProgram,
      isViewable = isViewable).init(obj)
  }

  override def initMakeDialog[T <: Txn[T]](window: Option[desktop.Window])
                                          (implicit universe: Universe[T]): MakeResult[T] = {
    val ggValue     = new TextField(20)
    ggValue.text    = "Value"
    val codeValue0  = "\"\""
    showMakeDialog(ggValue = ggValue, codeValue0 = codeValue0, prefix = prefix,
      window = window)(ggValue.text)
  }

  final class Impl[T <: Txn[T]](val objH: Source[T, StringObj[T]],
                                var value: String,
                                override val isListCellEditable: Boolean, val isProgram: Boolean,
                                val isViewable: Boolean)
    extends StringObjView[T] with ListBase[T]
      with ObjListViewImpl.SimpleExpr[T, Elem, E] with StringRenderer {

    override def convertEditValue(v: Any): Option[String] = Some(v.toString)
  }
}
trait StringObjView[T <: LTxn[T]] extends ObjView[T] {
  type Repr = StringObj[T]
}