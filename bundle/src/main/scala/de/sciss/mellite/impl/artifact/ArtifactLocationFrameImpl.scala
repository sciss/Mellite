/*
 *  ArtifactLocationFrameImpl.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite.impl.artifact

import de.sciss.lucre.ArtifactLocation
import de.sciss.lucre.edit.UndoManager
import de.sciss.lucre.expr.CellView
import de.sciss.lucre.synth.Txn
import de.sciss.mellite.ArtifactLocationObjView.humanName
import de.sciss.mellite.impl.WorkspaceWindowImpl
import de.sciss.mellite.{ArtifactLocationFrame, ArtifactLocationView, UniverseHandler}

object ArtifactLocationFrameImpl {
  def apply[T <: Txn[T]](obj: ArtifactLocation[T])
                        (implicit tx: T, handler: UniverseHandler[T]): ArtifactLocationFrame[T] =
    handler(obj, ArtifactLocationFrame)(newInstance(obj))

  private def newInstance[T <: Txn[T]](obj: ArtifactLocation[T])
                                      (implicit tx: T, handler: UniverseHandler[T]): ArtifactLocationFrame[T] = {
    implicit val undoMgr: UndoManager[T] = UndoManager()
    import handler.universe
    val afv       = ArtifactLocationView(obj)
    val name      = CellView.name(obj)
    val res       = new Impl(afv)
    res.init().setTitle(name.map(n => s"$n : $humanName"))
  }

  private final class Impl[T <: Txn[T]](val view: ArtifactLocationView[T])
                                       (implicit val handler: UniverseHandler[T])
    extends WorkspaceWindowImpl[T] with ArtifactLocationFrame[T] {

    override def newWindow()(implicit tx: T): ArtifactLocationFrame[T] =
      newInstance(view.obj)
  }
}