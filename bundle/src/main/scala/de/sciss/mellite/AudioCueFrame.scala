/*
 *  AudioCueFrame.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite

import de.sciss.lucre.synth.Txn
import de.sciss.lucre.{Txn => LTxn}
import de.sciss.mellite.impl.audiocue.{AudioCueFrameImpl => Impl}
import de.sciss.proc.AudioCue

object AudioCueFrame extends WorkspaceWindow.Key {
  def apply[T <: Txn[T]](obj: AudioCue.Obj[T])
                        (implicit tx: T, handler: UniverseHandler[T]): AudioCueFrame[T] =
    Impl(obj)

  type Repr[T <: LTxn[T]] = AudioCueFrame[T]
}

trait AudioCueFrame[T <: LTxn[T]] extends WorkspaceWindow[T] {
  type Repr[~ <: LTxn[~]] = AudioCueFrame[~]

  override def key: Key = AudioCueFrame

  override def view: AudioCueView[T]
}
