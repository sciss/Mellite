/*
 *  EditArtifactLocation.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite.edit

import de.sciss.lucre.edit.UndoableEdit
import de.sciss.lucre.edit.impl.BasicUndoableEdit
import de.sciss.lucre.{ArtifactLocation, Source, Txn}

import java.net.URI

object EditArtifactLocation {
  def apply[T <: Txn[T]](obj: ArtifactLocation.Var[T], directory: URI)
                        (implicit tx: T): UndoableEdit[T] = {
    val before    = obj.directory
    val objH      = tx.newHandle(obj)
    val res       = new Impl(objH, before, directory)
    res.perform()
    res
  }

  private[edit] final class Impl[T <: Txn[T]](objH  : Source[T, ArtifactLocation.Var[T]],
                                              before: URI, now: URI)
    extends BasicUndoableEdit[T] {

    override protected def undoImpl()(implicit tx: T): Unit =
      perform(before)

    override protected def redoImpl()(implicit tx: T): Unit =
      perform()

    private def perform(directory: URI)(implicit tx: T): Unit =
      objH().update(ArtifactLocation.newConst(directory)) // .directory = directory

    def perform()(implicit tx: T): Unit = perform(now)

    override def name: String = "Change Artifact Location"
  }
}
