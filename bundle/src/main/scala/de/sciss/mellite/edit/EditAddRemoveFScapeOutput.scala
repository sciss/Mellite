/*
 *  EditAddRemoveFScapeOutput.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite.edit

import de.sciss.lucre.edit.UndoManager
import de.sciss.lucre.edit.impl.BasicUndoableEdit
import de.sciss.lucre.{Obj, Source, Txn => LTxn}
import de.sciss.proc.FScape

// direction: true = insert, false = remove
// XXX TODO - should disconnect links and restore them in undo
private[edit] final class EditAddRemoveFScapeOutput[T <: LTxn[T]](isAdd: Boolean,
                                                                  fscapeH: Source[T, FScape[T]],
                                                                  key: String, tpe: Obj.Type)
  extends BasicUndoableEdit[T] {

  override protected def undoImpl()(implicit tx: T): Unit =
    perform(isUndo = true)

  override protected def redoImpl()(implicit tx: T): Unit =
    perform()

  def perform()(implicit tx: T): Unit = perform(isUndo = false)

  private def perform(isUndo: Boolean)(implicit tx: T): Unit = {
    val fscape = fscapeH()
    val outputs = fscape.outputs
    if (isAdd ^ isUndo)
      outputs.add   (key, tpe)
    else
      outputs.remove(key)
  }

  override def name: String = s"${if (isAdd) "Add" else "Remove"} Output"
}
object EditAddFScapeOutput {
  def apply[T <: LTxn[T]](fscape: FScape[T], key: String, tpe: Obj.Type)
                         (implicit tx: T, undoManager: UndoManager[T]): Unit = {
    val fscapeH = tx.newHandle(fscape)
    val res = new EditAddRemoveFScapeOutput[T](isAdd = true, fscapeH = fscapeH, key = key, tpe = tpe)
    res.perform()
    undoManager.addEdit(res)
  }
}

object EditRemoveFScapeOutput {
  def apply[T <: LTxn[T]](output: FScape.Output[T])
                         (implicit tx: T, undoManager: UndoManager[T]): Unit = {
    val fscape  = output.fscape
    val key     = output.key
    val tpe     = output.tpe
    val fscapeH = tx.newHandle(fscape)
    val res = new EditAddRemoveFScapeOutput[T](isAdd = false, fscapeH = fscapeH, key = key, tpe = tpe)
    res.perform()
    undoManager.addEdit(res)
  }
}