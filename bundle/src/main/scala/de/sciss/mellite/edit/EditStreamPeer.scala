/*
 *  EditStreamPeer.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite.edit

import de.sciss.lucre.edit.UndoManager
import de.sciss.lucre.edit.impl.BasicUndoableEdit
import de.sciss.lucre.{Source, Txn}
import de.sciss.patterns.lucre.{Context => LContext, Stream => LStream}
import de.sciss.patterns.{Context => PContext, Stream => PStream}

object EditStreamPeer {
  def apply[T <: Txn[T]](name: String, stream: LStream[T], value: PStream[T, Any])
                        (implicit tx: T, undoManager: UndoManager[T]): Unit = {
    val streamH = tx.newHandle(stream)
    implicit val ctx: PContext[T] = LContext[T]()
//    implicitly[Serializer[T, S#Acc, PStream[T, Any]]]
    val beforeH = tx.newHandle(stream.peer())
    val nowH    = tx.newHandle(value)
    val res     = new Impl(name, streamH, beforeH, nowH)
    res.perform()
    undoManager.addEdit(res)
  }

  private final class Impl[T <: Txn[T]](val name: String,
                                        streamH: Source[T, LStream[T]],
                                        beforeH: Source[T, PStream[T, Any]],
                                        nowH   : Source[T, PStream[T, Any]])
    extends BasicUndoableEdit[T] {

    override protected def undoImpl()(implicit tx: T): Unit = {
      val stream    = streamH()
      stream.peer() = beforeH()
    }

    override protected def redoImpl()(implicit tx: T): Unit =
      perform()

    def perform()(implicit tx: T): Unit = {
      val stream    = streamH()
      stream.peer() = nowH()
    }
  }
}