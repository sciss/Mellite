/*
 *  EditAddRemoveOutput.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite.edit

import de.sciss.lucre.edit.UndoManager
import de.sciss.lucre.edit.impl.BasicUndoableEdit
import de.sciss.lucre.{Source, Txn}
import de.sciss.proc.Proc

// direction: true = insert, false = remove
// XXX TODO - should disconnect links and restore them in undo
private[edit] final class EditAddRemoveProcOutput[T <: Txn[T]](isAdd: Boolean,
                                                               procH: Source[T, Proc[T]],
                                                               key: String)
  extends BasicUndoableEdit[T] {

  override protected def undoImpl()(implicit tx: T): Unit =
    perform(isUndo = true)

  override protected def redoImpl()(implicit tx: T): Unit =
    perform()

  def perform()(implicit tx: T): Unit = perform(isUndo = false)

  private def perform(isUndo: Boolean)(implicit tx: T): Unit = {
    val proc    = procH()
    val outputs = proc.outputs
    if (isAdd ^ isUndo)
      outputs.add   (key)
    else
      outputs.remove(key)
  }

  override def name: String = s"${if (isAdd) "Add" else "Remove"} Output"
}
object EditAddProcOutput {
  def apply[T <: Txn[T]](proc: Proc[T], key: String)
                        (implicit tx: T, undo: UndoManager[T]): Unit = {
    val procH = tx.newHandle(proc)
    val res = new EditAddRemoveProcOutput(isAdd = true, procH = procH, key = key)
    res.perform()
    undo.addEdit(res)
  }
}

object EditRemoveProcOutput {
  def apply[T <: Txn[T]](proc: Proc[T], key: String)
                        (implicit tx: T, undo: UndoManager[T]): Unit = {
    val procH = tx.newHandle(proc)
    val res = new EditAddRemoveProcOutput(isAdd = false, procH = procH, key = key)
    res.perform()
    undo.addEdit(res)
  }
}