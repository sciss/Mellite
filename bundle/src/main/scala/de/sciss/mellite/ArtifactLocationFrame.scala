/*
 *  ArtifactLocationFrame.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite

import de.sciss.lucre.ArtifactLocation
import de.sciss.lucre.synth.Txn
import de.sciss.lucre.{Txn => LTxn}
import de.sciss.mellite.impl.artifact.{ArtifactLocationFrameImpl => Impl}

object ArtifactLocationFrame extends WorkspaceWindow.Key {
  def apply[T <: Txn[T]](obj: ArtifactLocation[T])
                        (implicit tx: T, handler: UniverseHandler[T]): ArtifactLocationFrame[T] =
    Impl(obj)

  type Repr[T <: LTxn[T]] = ArtifactLocationFrame[T]
}

trait ArtifactLocationFrame[T <: LTxn[T]] extends WorkspaceWindow[T] {
  type Repr[~ <: LTxn[~]] = ArtifactLocationFrame[~]

  override def key: Key = ArtifactLocationFrame

  override def view: ArtifactLocationView[T]
}
