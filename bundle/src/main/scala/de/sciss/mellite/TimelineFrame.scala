/*
 *  TimelineFrame.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite

import de.sciss.lucre.synth.Txn
import de.sciss.lucre.{Txn => LTxn}
import de.sciss.mellite.impl.timeline.{TimelineFrameImpl => Impl}
import de.sciss.proc.Timeline

object TimelineFrame extends WorkspaceWindow.Key {
  def apply[T <: Txn[T]](obj: Timeline[T])
                        (implicit tx: T, handler: UniverseHandler[T]): TimelineFrame[T] =
    Impl(obj)

  type Repr[T <: LTxn[T]] = TimelineFrame[T]
}
trait TimelineFrame[T <: LTxn[T]] extends WorkspaceWindow[T] {
  type Repr[~ <: LTxn[~]] = TimelineFrame[~]

  override def view: TimelineView[T]
}