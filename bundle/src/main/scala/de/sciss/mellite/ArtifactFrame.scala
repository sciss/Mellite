/*
 *  ArtifactFrame.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite

import de.sciss.desktop.FileDialog
import de.sciss.lucre.Artifact
import de.sciss.lucre.synth.Txn
import de.sciss.lucre.{Txn => LTxn}
import de.sciss.mellite.impl.artifact.ArtifactFrameImpl

object ArtifactFrame extends WorkspaceWindow.Key {
  def apply[T <: Txn[T]](obj: Artifact[T], mode: Boolean, initMode: FileDialog.Mode = FileDialog.Save)
                        (implicit tx: T, handler: UniverseHandler[T]): ArtifactFrame[T] =
    ArtifactFrameImpl(obj, mode = mode, initMode = initMode)

  type Repr[T <: LTxn[T]] = ArtifactFrame[T]
}

trait ArtifactFrame[T <: LTxn[T]] extends WorkspaceWindow[T] {
  type Repr[~ <: LTxn[~]] = ArtifactFrame[~]

  def key: Key = ArtifactFrame

  override def view: ArtifactView[T]
}
