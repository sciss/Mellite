/*
 *  Bounds.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite.impl

final case class Bounds(x: Int, y: Int, width: Int, height: Int) {
  def toVector: Vector[Int] = Vector(x, y, width, height)
}