/*
 *  WorkspaceWindowImpl.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite.impl

import de.sciss.desktop.Menu
import de.sciss.desktop.Menu.Item
import de.sciss.lucre.swing.{UndoRedo, View}
import de.sciss.lucre.synth.Txn
import de.sciss.lucre.{Cursor, IntVector}
import de.sciss.mellite.{Application, GUI, Prefs, UniverseObjView, ViewState, WorkspaceWindow}
import de.sciss.proc.Tag
import de.sciss.synth.UGenSource.Vec

import java.awt.event.{ComponentEvent, ComponentListener}
import scala.concurrent.Future
import scala.swing.Action

trait WorkspaceWindowImpl[T <: Txn[T]] extends WorkspaceWindow[T] with WindowImpl[T] {
  @volatile
  private var lastViewState = Set.empty[ViewState]

  private var dirtyBounds = false

  override def view: UniverseObjView[T]

  @volatile
  private var stateBounds: Bounds = _

  def supportsNewWindow: Boolean = true

  private[this] var _undoRedo = Option.empty[UndoRedo[T]]

  override protected def undoRedo: Option[UndoRedo[T]] = _undoRedo

  protected def undoRedoActions: Option[(Action, Action)] = undoRedo.map(ur => (ur.undoAction, ur.redoAction))

  final override def init()(implicit tx: T): this.type = {
    stateBounds = (for {
      tAttr   <- ViewState.map(view.obj, key = viewStateKey)
      bounds  <- tAttr.$[IntVector](WindowImpl.StateKey_Bounds)
    } yield {
      bounds.value match {
        case Vec(x, y, width, height) => Bounds(x, y, width, height)
        case _ => null
      }
    }).orNull

    view match {
      case ve: View.Editable[T] =>
        _undoRedo = Some(UndoRedo[T]()(tx, ve.cursor, ve.undoManager))
      case _ => ()
    }

    handler.add(this)
    super.init()
  }

  override protected def packAndPlace: Boolean = stateBounds == null

  final protected def viewState: Set[ViewState] = view.viewState

  protected def viewStateKey: String = ViewState.Key_Base

  protected final def saveViewState(): Unit = {
    val b = Prefs.viewSaveState.getOrElse(false)
    if (b) {
      val state0  = viewState
      val state1  = if (!dirtyBounds) state0 else {
        val entry = ViewState(WindowImpl.StateKey_Bounds, IntVector, stateBounds.toVector)
        state0 + entry
      }
      lastViewState = state1
    }
  }

  override protected def performClose(): Future[Unit] = {
    saveViewState()
    super.performClose()
  }

  override def dispose()(implicit tx: T): Unit = {
    if (!wasDisposed) {
      if (lastViewState.nonEmpty) {
        val viewObj = view.obj
        val attr    = viewObj.attr
        val tag     = attr.$[Tag](viewStateKey).getOrElse {
          val t = Tag[T]()
          attr.put(viewStateKey, t)
          t
        }
        val tAttr = tag.attr
        lastViewState.foreach(_.set(tAttr))
      }
      handler.remove(this)
    }
    super.dispose()
  }

  private def newWindowGUI(): Unit = {
    implicit val cursor: Cursor[T] = handler.universe.cursor
    GUI.step[T]("New View", "Opening window", window = Some(window)) { implicit tx =>
      newWindow()
    }
  }

  override protected def initGUI(): Unit = {
    if (supportsNewWindow) {
      val mf = Application.windowHandler.menuFactory
      mf.get("file.new").foreach {
        case g: Menu.Group =>
          g.add(Some(window), Item("view", Action("View")(newWindowGUI())))
        case _ =>
      }
    }

    if (!packAndPlace) {
      val b = stateBounds
      val j = window.component.peer
      if (resizable) {
        // pack()
        j.setBounds(b.x, b.y, b.width, b.height)
      } else {
        pack()
        j.setLocation(b.x, b.y)
      }
    }

    val rp = window.component
    rp.peer.addComponentListener(new ComponentListener {
      private var minBoundTime = Long.MaxValue

      private def updateBounds(e: ComponentEvent): Unit = {
        val r = e.getComponent.getBounds
        val b = Bounds(r.x, r.y, r.width, r.height)
        if (stateBounds != b) {
          stateBounds = b
          val ok = System.currentTimeMillis() > minBoundTime
          // println(s"updateBounds: $stateBounds - $ok")
          if (ok) dirtyBounds = true
        }
      }

      override def componentResized (e: ComponentEvent): Unit = updateBounds(e)
      override def componentMoved   (e: ComponentEvent): Unit = updateBounds(e)
      override def componentShown   (e: ComponentEvent): Unit = {
        minBoundTime = System.currentTimeMillis() + 500  // XXX TODO
      }

      override def componentHidden(e: ComponentEvent): Unit = ()
    })
  }
}
