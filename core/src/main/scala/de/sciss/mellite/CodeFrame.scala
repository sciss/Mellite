/*
 *  CodeFrame.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite

import de.sciss.lucre.swing.View
import de.sciss.lucre.synth.Txn
import de.sciss.lucre.{Expr, Txn => LTxn}
import de.sciss.proc.{Action, Code, Control, Proc}

import scala.collection.immutable.{Seq => ISeq}

object CodeFrame extends WorkspaceWindow.Key {
  private[mellite] var peer: Companion = _

  private def companion: Companion = {
    require (peer != null, "Companion not yet installed")
    peer
  }

  private[mellite] trait Companion {
    def apply[T <: Txn[T]](obj: Code.Obj[T], bottom: ISeq[View[T]])
                          (implicit tx: T, handler: UniverseHandler[T],
                           compiler: Code.Compiler): CodeFrame[T]

    def proc[T <: Txn[T]](proc: Proc[T])
                         (implicit tx: T, handler: UniverseHandler[T],
                          compiler: Code.Compiler): CodeFrame[T]

    def control[T <: Txn[T]](control: Control[T])
                           (implicit tx: T, handler: UniverseHandler[T],
                            compiler: Code.Compiler): CodeFrame[T]

    def action[T <: Txn[T]](action: Action[T])
                            (implicit tx: T, handler: UniverseHandler[T],
                             compiler: Code.Compiler): CodeFrame[T]

    def program[T <: Txn[T], A, E[~ <: LTxn[~]] <: Expr[~, A]](obj: E[T] with Expr.Program[T, A])
                                                             (implicit tx: T, universeHandler: UniverseHandler[T],
                                                              compiler: Code.Compiler,
                                                              tpe: Expr.Type[A, E]): CodeFrame[T]
  }

  def apply[T <: Txn[T]](obj: Code.Obj[T], bottom: ISeq[View[T]])
                        (implicit tx: T, handler: UniverseHandler[T],
                         compiler: Code.Compiler): CodeFrame[T] =
    companion(obj, bottom = bottom)

  def proc[T <: Txn[T]](proc: Proc[T])
                        (implicit tx: T, handler: UniverseHandler[T],
                         compiler: Code.Compiler): CodeFrame[T] =
    companion.proc(proc)

  def control[T <: Txn[T]](control: Control[T])
                          (implicit tx: T, handler: UniverseHandler[T],
                           compiler: Code.Compiler): CodeFrame[T] =
    companion.control(control)

  def action[T <: Txn[T]](action: Action[T])
                         (implicit tx: T, handler: UniverseHandler[T],
                          compiler: Code.Compiler): CodeFrame[T] =
    companion.action(action)

  def program[T <: Txn[T], A, E[~ <: LTxn[~]] <: Expr[~, A]](obj: E[T] with Expr.Program[T, A])
                                                            (implicit tx: T, universeHandler: UniverseHandler[T],
                                                             compiler: Code.Compiler,
                                                             tpe: Expr.Type[A, E]): CodeFrame[T] =
    companion.program(obj)

  type Repr[T <: LTxn[T]] = CodeFrame[T]
}

trait CodeFrame[T <: LTxn[T]] extends WorkspaceWindow[T] {
  type Repr[~ <: LTxn[~]] = CodeFrame[~]

  override def key: Key = CodeFrame

  def codeView: CodeView[T, _]
}
