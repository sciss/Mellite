/*
 *  CodeView.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite

import de.sciss.lucre.edit.UndoManager
import de.sciss.lucre.swing.View
import de.sciss.lucre.synth.Txn
import de.sciss.lucre.{Disposable, TxnLike, Txn => LTxn}
import de.sciss.model.Model
import de.sciss.proc.{Code, Universe}

import scala.collection.immutable.{Seq => ISeq}
import scala.concurrent.Future
import scala.swing.Action

object CodeView {
  private[mellite] var peer: Companion = _

  private def companion: Companion = {
    require (peer != null, "Companion not yet installed")
    peer
  }

  private[mellite] trait Companion {
    def apply[T <: Txn[T]](obj: Code.Obj[T], code0: Code, built0: Boolean, bottom: ISeq[View[T]], numLines: Int)
                          (handler: Option[Handler[T, code0.In, code0.Out]])
                          (implicit tx: T, universe: Universe[T],
                           compiler: Code.Compiler,
                           undoManager: UndoManager[T]): CodeView[T, code0.Out]

    def availableFonts(): ISeq[String]

    def installFonts(): Unit
  }

  trait Handler[T <: LTxn[T], In, -Out] extends Disposable[T] {
    def in(): In
    def save(in: In, out: Out)(implicit tx: T, undoManager: UndoManager[T]): Unit
  }

  /** Creates a new `CodeView`.
    *
    * @param obj          the object that contains the code
    * @param code0        the initial code, either from `obj` or from a swap object
    * @param built0       `true` if the current code is corresponds with a built object,
    *                     `false` if not (e.g. if swap source is used)
    * @param bottom       additional components to render at the bottom of the view
    * @param handler      handler responsible for building the underlying object from source
    */
  def apply[T <: Txn[T]](obj: Code.Obj[T], code0: Code, built0: Boolean, bottom: ISeq[View[T]], numLines: Int = 24)
                        (handler: Option[Handler[T, code0.In, code0.Out]])
                        (implicit tx: T, universe: Universe[T],
                         compiler: Code.Compiler,
                         undoManager: UndoManager[T]): CodeView[T, code0.Out] =
    companion(obj, code0, built0 = built0, bottom = bottom, numLines = numLines)(handler)

  sealed trait Update
  case class DirtyChange      (value: Boolean) extends Update
  case class VisibilityChange (value: Boolean) extends Update
  case class BuiltChange      (value: Boolean) extends Update

  def availableFonts(): ISeq[String] = companion.availableFonts()

  def installFonts(): Unit = companion.installFonts()

  /** If present, indicates that a "swap" version of the code object
    * exist (in the value), which does not correspond to the built object.
    */
  final val attrSwap = "swap"
}
trait CodeView[T <: LTxn[T], Out] extends UniverseObjView[T] with Model[CodeView.Update] {
  override def obj(implicit tx: T): Code.Obj[T]

  def isCompiling(implicit tx: TxnLike): Boolean

  /** Whether the current editor content is dirty, that is unsaved. */
  def dirty(implicit tx: TxnLike): Boolean

  /** Whether the saved source code corresponds with the built object. This relates
    * to the clean state, so if `dirty` returns `true`, it may still be the case
    * that the last saved state was built and thus this method returns `true`. If
    * the method returns `false`, it means that the last saved source code is in
    * the "swap" attribute entry.
    */
  def built(implicit tx: TxnLike): Boolean

  /** Call on EDT outside Txn */
  def visible: Boolean

  /** Requests to store the dirty editor content in the source object and update the
    * compiled and built object.
    * If this fails, or the object should not be built, the `saveSwap` method can be used.
    * If this succeeds, the "swap" attribute entry is deleted if it existed.
    *
    * Call this method on EDT outside Txn
    */
  def save(): Future[Unit]

  /** Requests to store the dirty editor content in the swap entry of the source object's attribute map.
    *
    * Call this method on EDT outside Txn
    *
    * @see [[CodeView.attrSwap]]
    */
  def saveSwap(): Future[Unit] = throw new NotImplementedError()  // XXX TODO remove dummy impl in major version

  def preview(): Future[Out]

  var currentText: String

  // def updateSource(text: String)(implicit tx: T): Unit

  def undoAction: Action
  def redoAction: Action
}