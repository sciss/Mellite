/*
 *  WorkspaceWindow.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite

import de.sciss.lucre.swing.Window
import de.sciss.lucre.{Txn => LTxn}

object WorkspaceWindow {
  trait Key {
    type Repr[~ <: LTxn[~]] <: WorkspaceWindow[~]
  }
  type T[R[~ <: LTxn[~]]] = Key { type Repr[~ <: LTxn[~]] = R[~] }
}
trait WorkspaceWindow[T <: LTxn[T]] extends Window[T] {
  override def view: UniverseObjView[T]

  type Repr[~ <: LTxn[~]] <: WorkspaceWindow[~]

  type Key = WorkspaceWindow.T[Repr]

  def key: Key

  def handler: UniverseHandler[T]

  /** Whether the `newWindow` method is supported. */
  def supportsNewWindow: Boolean

  /** Creates a new window of the same type, viewing the same underlying object.
    * May throw an exception if `supportsNewWindow` returns `false`.
    */
  def newWindow()(implicit tx: T): Repr[T]
}
