/*
 *  UniverseHandler.scala
 *  (Mellite)
 *
 *  Copyright (c) 2012-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.mellite

import de.sciss.lucre.swing.LucreSwing.deferTx
import de.sciss.lucre.synth.Txn
import de.sciss.lucre.{Disposable, Obj, Txn => LTxn}
import de.sciss.mellite.WorkspaceWindow.Key
import de.sciss.proc.Universe

object UniverseHandler {
  private final val DEBUG = false

  /** Creates a new universe handler that will be disposed with the universe's workspace. */
  def apply[T <: Txn[T]]()(implicit universe: Universe[T], tx: T): UniverseHandler[T] = {
    val res = new Impl[T](tx)
    universe.workspace.addDependent(res)
    res
  }

  private final class Impl[T <: Txn[T]](tx0: T)(implicit val universe: Universe[T])
    extends UniverseHandler[T] with Disposable[T] {

    private[this] val map = tx0.newIdentMap[List[WorkspaceWindow[T]]]

    override def find(obj: Obj[T], key: Key)(implicit tx: T): List[key.Repr[T]] =
      map.getOrElse(obj.id, Nil).collect {
        case w if w.key == key => w.asInstanceOf[key.Repr[T]]
      }

    override def add(w: WorkspaceWindow[T])(implicit tx: T): w.type = {
      val obj     = w.view.obj
      val id      = obj.id
      val listOld = map.getOrElse(id, Nil)
      if (listOld.contains(w)) throw new IllegalArgumentException(s"Trying to add window $w twice")
      val listNew = w :: listOld
      if (DEBUG) println(s"UH added $w for $obj - list size ${listNew.size}")
      map.put(id, listNew)
      w
    }

    override def remove(w: WorkspaceWindow[T])(implicit tx: T): Unit = {
      val obj     = w.view.obj
      val id      = obj.id
      val listOld = map.getOrElse(id, Nil)
      val listNew = listOld.diff(w :: Nil)
      if (DEBUG) println(s"UH removed $w for $obj - list size ${listNew.size}")
      if (listNew.isEmpty) map.remove(id) else map.put(id, listNew)
    }

    override def apply(obj: Obj[T], key: Key)(w: => key.Repr[T])(implicit tx: T): key.Repr[T] =
      find(obj, key) match {
        case head :: _ =>
          if (DEBUG) println(s"UH found $head for $obj and $key")
          deferTx {
            head.window.front()
          }
          head
        case Nil =>
          val res = w
          if (DEBUG) println(s"UH created $res for $obj and $key")
          res
      }

    override def dispose()(implicit tx: T): Unit =
      map.dispose()
  }
}
/** The UI handler for a universe (and thus a workspace). */
trait UniverseHandler[T <: LTxn[T]] /*extends Disposable[T]*/ {
  implicit val universe: Universe[T]

  def find(obj: Obj[T], key: Key)(implicit tx: T): List[key.Repr[T]]

  def add(w: WorkspaceWindow[T])(implicit tx: T): w.type

  def remove(w: WorkspaceWindow[T])(implicit tx: T): Unit

  /** Looks for an existing window via `find`. If found, this window is fronted
    * and returned. If not found, a new instance is created via `w`. Such new instance
    * must make sure it calls `add` itself!
    */
  def apply(obj: Obj[T], key: Key)(w: => key.Repr[T])(implicit tx: T): key.Repr[T]
}
